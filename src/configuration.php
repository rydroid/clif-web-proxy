<?php require_once('kernel.inc.php'); ?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Configuration - RyDroid Web Proxy</title>
    
    <link
    	rel="stylesheet" media="all" type="text/css"
    	href="include/css/design.css" />
    <link rel="icon" type="image/png" href="favicon.png" />
  </head>
  
  <body>
    <header>
      <a href="./" title="back">←</a>
      <h1>Configuration</h1>
    </header>
    
    <h2 id="cookies">Cookies</h2>
    
    <noscript>JavaScript is needed!</noscript>

    <?php if(UserInterfaceConfigurationGeneral::
             showProxyCookiesConfiguration()) { ?>
    <h3 id="cookies-proxy">Proxy</h3>
    
    <form method="get" action="configuration-proxy-set.php"
          id="cookies-proxy-form">
      <p>
        <label for="proxy-url">URL of the proxy</label>
        <input id="proxy-url" name="proxy-url" type="text"
               <?php
               if(ProxyConfigurationGeneral::hasUrl())
               {
                 echo 'value="';
                 echo ProxyConfigurationGeneral::getUrl();
                 echo '"';
               }
               ?> />
        <span id="proxy-url-save-state"></span>
      </p>
      
      <p>
        <label for="proxy-port">Port number of proxy</label>
        <input id="proxy-port" name="proxy-port"
               type="number" min="0" max="65535" step="1"
               <?php
               if(ProxyConfigurationGeneral::hasPort())
               {
                   echo 'value="';
                   echo ProxyConfigurationGeneral::getPort();
                   echo '"';
               }
               ?> />
        <span id="proxy-port-save-state"></span>
      </p>
      
      <input type="submit" value="Save"  id="cookies-proxy-submit" />
      <input type="reset"  value="Reset" id="cookies-proxy-reset" />
      <span id="proxy-save-state"></span>
    </form>
    <?php } ?>
    
    <h3 id="cookies-session">Session captured</h3>
    
    <form method="get" action="configuration-session-set.php"
          id="cookies-session-form">
      <ul class="list-no-style list-position-inside no-padding-left">
        <li>
          <input id="session-save" name="session-save" type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSave())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save">Save session</label>
          <span id="session-save-state"></span>
        </li>
        
        <li>
          <input id="session-save-proxy" name="session-save-proxy"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveProxy())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-proxy">Save proxy</label>
          <span id="session-save-proxy-state"></span>
        </li>
        
        <li>
          <input id="session-save-cookies" name="session-save-cookies"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveCookies())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-cookies">Save cookies</label>
          <span id="session-save-cookies-state"></span>
        </li>
        
        <li>
          <input id="session-save-user-agent" name="session-save-user-agent"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveUserAgent())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-user-agent">Save user-agent</label>
          <span id="session-save-user-agent-state"></span>
        </li>
        
        <li>
          <input id="session-save-referer" name="session-save-referer"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveReferer())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-referer">Save referer</label>
          <span id="session-save-referer-state"></span>
        </li>
        
        <li>
          <input id="session-save-language" name="session-save-language"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveLanguage())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-language">Save language</label>
          <span id="session-save-language-state"></span>
        </li>
        
        <li>
          <input id="session-save-encoding" name="session-save-encoding"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveEncoding())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-encoding">Save encoding</label>
          <span id="session-save-encoding-state"></span>
        </li>
        
        <li>
          <input id="session-save-etag" name="session-save-etag"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveEtag())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-etag">Save HTTP ETag</label>
          <span id="session-save-etag-state"></span>
        </li>
        
        <li>
          <input id="session-save-dnt" name="session-save-dnt"
                 type="checkbox"
                 <?php
                 if(SessionCapturedConfigurationGeneral::hasToSaveDoNotTrack())
                 {
                   echo 'checked="checked"';
                 }
                 ?> />
          <label for="session-save-dnt">
            Save <abbr title="Do Not Track">DNT</abbr>
          </label>
          <span id="session-save-dnt-state"></span>
        </li>
      </ul>
      
      <input type="submit" value="Save" id="cookies-session-submit" />
    </form>
    
    <h2 id="current-configuration">Current configuration</h2>
    
    <h3 id="current-general-configuration">Current general configuration</h3>
    
    <div id="current-general-configuration-content">
      <?php
      if(class_exists('ConfigurationPrinterUtils'))
      {
        ConfigurationPrinterUtils::
        printAsHtmlList(new ConfigurationGeneral());
      }
      ?>
    </div>
    
    <?php if(UserInterfaceConfigurationGeneral::showDebug()) { ?>
    <h3 id="current-cookies-configuration">Current cookies configuration</h3>
    
    <div id="current-cookies-configuration-content">
      <?php ConfigurationPrinterUtils::
            printAsHtmlList(new ConfigurationCookies()); ?>
    </div>
    
    <h3 id="default-configuration">Default configuration</h3>
    
    <div id="default-configuration-content">
      <?php ConfigurationPrinterUtils::
            printAsHtmlList(new ConfigurationDefaultValues()); ?>
    </div>
    <?php } ?>
    
    <?php if(UserInterfaceConfigurationGeneral::useClientScripts()) { ?>
    <script type="text/javascript"
            src="include/js/utils/XMLHttpRequest.js"></script>
    <script type="text/javascript"
            src="include/js/utils/cookies.js"></script>
    <script type="text/javascript"
            src="include/js/specific/configuration-update-html.js"></script>
    <script type="text/javascript"
            src="include/js/specific/configuration-proxy.js"></script>
    <script type="text/javascript"
            src="include/js/specific/configuration-main.js"></script>
    <?php } ?>
  </body>
</html>
