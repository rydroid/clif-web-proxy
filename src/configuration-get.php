<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


require_once('kernel.inc.php');


const FROM_STRING_DEFAULT = 'general';
$fromString = null;
if(isset($_GET) &&
   !empty($_GET) &&
   isset($_GET['from']) &&
   !empty(trim($_GET['from'])))
{
    $fromString = strtolower(trim($_GET['from']));
}
else if(isset($_POST) &&
        !empty($_POST) &&
        isset($_POST['from']) &&
        !empty(trim($_POST['from'])))
{
    $fromString = strtolower(trim($_POST['from']));
}
else
{
    $fromString = FROM_STRING_DEFAULT;
}

$configuration = null;
if($fromString == 'general' ||
   $fromString == 'général' || $fromString == 'générale')
{
    $configuration = new ConfigurationGeneral();
}
else if($fromString == 'cookies' || $fromString == 'cookie')
{
    $configuration = new ConfigurationCookies();
}
else if($fromString == 'system-file' || $fromString == 'sys-file')
{
    $configuration = ConfigurationDefaultSystemFile::get();
}
else if($fromString == 'default-file')
{
    $configuration = ConfigurationDefaultFile::get();
}
else if($fromString == 'default' || $fromString == 'defaut')
{
    $configuration = new ConfigurationDefaultValues();
}
else
{
    exit(0);
}

const DEFAULT_FORMAT = 'html';
$format = null;
if(isset($_GET) &&
   !empty($_GET) &&
   isset($_GET['format']) &&
   !empty(trim($_GET['format'])))
{
    $format = strtolower(trim($_GET['format']));
}
else if(isset($_POST) &&
        !empty($_POST) &&
        isset($_POST['format']) &&
        !empty(trim($_POST['format'])))
{
    $format = strtolower(trim($_POST['format']));
}
else
{
    $format = DEFAULT_FORMAT;
}

if($format == 'html' || $format == 'xhtml')
{
    echo ConfigurationPrinterUtils::printAsHtmlList($configuration);
}
