<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class RedirectUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getOptionnalBooleanFromMap(array $aMap) : OptionnalBoolean
    {
        if(empty($aMap) || !isset($aMap['redirect']))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return OptionnalBooleanUtils::createFromString($aMap['redirect']);
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        getOptionnalBooleanFromGetParameters() : OptionnalBoolean
    {
        if(!isset($_GET))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return self::getOptionnalBooleanFromMap($_GET);
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        getOptionnalBooleanFromPostParameters() : OptionnalBoolean
    {
        if(!isset($_POST))
        {
            return OptionnalBooleanUtils::createUndefined();
        }
        return self::getOptionnalBooleanFromMap($_POST);
    }
    
    public static function
        getOptionnalBooleanFromHttpParameters() : OptionnalBoolean
    {
        $result = self::getOptionnalBooleanFromGetParameters();
        if($result->isDefined())
        {
            return $result;
        }
        return self::getOptionnalBooleanFromPostParameters();
    }
    
    public static function
        redirect(string $path) : bool
    {
        if(empty(trim($path)))
        {
            return false;
        }
        header('Location: '. $path);
        return true;
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        redirectWithReferer(string $defaultPath) : bool
    {
        if(isset($_SERVER) &&
           isset($_SERVER['HTTP_REFERER']) &&
           self::redirect($_SERVER['HTTP_REFERER']))
        {
            return true;
        }
        return self::redirect($defaultPath);
    }
}
