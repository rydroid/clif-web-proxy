<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class HttpSessionCapturedConfigurationMutableAbstract
    extends HttpSessionCapturedConfigurationAbstract
{
    public abstract function
        setSaveCookies(OptionnalBoolean $value);
    
    public function
        setSaveCookiesFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveCookies(OptionnalBooleanUtils::createFromString($value));
        return true;
    }
    
    
    public abstract function
        setSaveUserAgent(OptionnalBoolean $value);
    
    public function
        setSaveUserAgentFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveUserAgent(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setSaveReferer(OptionnalBoolean $value);
    
    public function
        setSaveRefererFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveReferer(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setSaveLanguage(OptionnalBoolean $value);
    
    public function
        setSaveLanguageFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveLanguage(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setSaveEncoding(OptionnalBoolean $value);
    
    public function
        setSaveEncodingFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveEncoding(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setSaveEtag(OptionnalBoolean $value);
    
    public function
        setSaveEtagFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveEtag(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setSaveDoNotTrack(OptionnalBoolean $value);
    
    public function
        setSaveDoNotTrackFromString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSaveDoNotTrack(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
}
