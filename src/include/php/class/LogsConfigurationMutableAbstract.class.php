<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class LogsConfigurationMutableAbstract
    extends LogsConfigurationAbstract
{
    public abstract function
        setSave(OptionnalBoolean $value);
    
    public function
        setSaveWithString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setSave(OptionnalBooleanUtils::createFromString($value));
        return true;
    }
    
    
    public abstract function
        setFilePath(string $value) : bool;
    
    public function
        setDirectoryName(string $value) : bool
    {
        if($value != '/' && StringUtils::endsWith($value , '/'))
        {
            $value = rtrim($value, '/');
        }
        if(empty(trim($value)))
        {
            return false;
        }
        return $this->setFilePath($value .'/'. $this->getBaseName());
    }
    
    public function
        setBaseName(string $value) : bool
    {
        if(empty(trim($value)))
        {
            return false;
        }
        return $this->setFilePath($this->getDirectoryName() .'/'. $value);
    }
    
    public function
        setFormatWithString(string $value) : bool
    {
        if(empty(trim($value)))
        {
            return false;
        }
        return $this->setFilePath(
            $this->getDirectoryName() .'/'. $this->getFileName() .'.'. $value
        );
    }
    
    
    public abstract function
        setShow(OptionnalBoolean $value);
    
    public function
        setShowWithString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setShow(OptionnalBooleanUtils::createFromString($value));
        return true;
    }
}
