<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class ProxyConfigurationConst
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const KEYS_FOR_URL = ['proxy-url', 'proxy-URL', 'url-of-proxy'];
    const DEFAULT_KEY_FOR_URL = self::KEYS_FOR_URL[0];
    
    const KEYS_FOR_PORT = ['proxy-port', 'proxy-port-number', 'port-for-proxy'];
    const DEFAULT_KEY_FOR_PORT = self::KEYS_FOR_PORT[0];
}
