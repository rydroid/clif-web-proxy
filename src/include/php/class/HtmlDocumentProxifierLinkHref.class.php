<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierLinkHref
    extends DomDocumentProxifierAttribute
{
    const ATTRIBUTE = 'href';
    const XPATH_QUERIES = [
        '/html/body//a'.
        '[not(starts-with(@href, \'ftp://\')) and '
        .'not(starts-with(@href, \'mailto:\')) and '
        .'not(starts-with(@href, \'tel:\')) and '
        .'not(starts-with(@href, \'magnet:\')) and '
        .'not(starts-with(@href, \'about:\')) and '
        .'not(starts-with(@href, \'javascript:\')) and '
        .'not(starts-with(@href, \'view-source:\'))]'
    ];
    
    public function
        __construct(DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct(
            self::ATTRIBUTE,
            self::XPATH_QUERIES,
            $previousProxy
        );
    }
}
