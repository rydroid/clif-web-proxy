<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class WebSessionCapturedFileLockedAbstract
    extends WebSessionCapturedFileAbstract
{
    private $lockFile;
    private $forceLock;
    
    
    public function
        __construct(string $filePath)
    {
        parent::__construct($filePath);
        $this->lockFile  = null;
        $this->forceLock
            = WebSessionCapturedFileLockedDefault::isForcingLock();
        $this->lockFileNameSuffix
            = WebSessionCapturedFileLockedDefault::getLockFileNameSuffix();
    }
    
    public function
        __destruct()
    {
        parent::__destruct();
        if($this->lockFile != null)
        {
            fclose($this->lockFile);
        }
    }
    
    
    public function
        getFileLockPath() : string
    {
        return $this->getFilePath() . $this->lockFileNameSuffix;
    }
    
    protected function
        lockFile() : array
    {
        if($this->lockFile == null || $this->lockFile == FALSE)
        {
            try
            {
                $this->lockFile = fopen($this->getFileLockPath(), 'w');
            }
            catch(Exception $exception)
            {
                return array((string) $exception);
            }
            if($this->lockFile == FALSE)
            {
                $this->lockFile = null;
                return array(
                    'Impossible to open file '. $this->getFileLockPath()
                );
            }
        }
        
        if(flock($this->lockFile, LOCK_EX))
        {
            return array();
        }
        return array('flock failed for '. $this->getFileLockPath());
    }
    
    protected function
        lockFileReturnSuccess() : bool
    {
        return empty($this->lockFile());
    }
    
    protected function
        unlockFile() : bool
    {
        if($this->lockFile == null || $this->lockFile == FALSE)
        {
            return false;
        }
        return flock($this->lockFile, LOCK_UN);
    }
    
    
    public function
        isForcingLock() : bool
    {
        return $this->forceLock;
    }
    
    public function
        setForceLock(bool $value)
    {
        $this->forceLock = $value;
    }
    
    public function
        getLockFileNameSuffix() : string
    {
        return $this->lockFileNameSuffix;
    }
    
    public function
        setLockFileNameSuffix(string $value) : bool
    {
        if($value == null ||
           strlen($value) < self::LOCK_FILE_NAME_SUFFIX_MIN_LENGTH)
        {
            return false;
        }
        $this->lockFileNameSuffix = $value;
        return true;
    }
    
    
    protected abstract function
        saveFileWithoutLock($file) : array;
    
    protected abstract function
        loadFileWithoutUnlock($file) : array;
    
    protected function
        saveFileWithoutLockReturnSuccess($file) : bool
    {
        return empty($this->saveFileWithoutLock($file));
    }
    
    protected function
        loadFileWithoutUnlockReturnSuccess($file) : bool
    {
        return empty($this->loadFileWithoutUnlock($file));
    }
    
    protected function
        saveFile($file) : array
    {
        $errors = $this->lockFile();
        if($this->isForcingLock() && !empty($errors))
        {
            $filePath = $this->getFilePath();
            $error = 'Impossible to lock the session for '. $filePath;
            $errors[] = $error;
            return $errors;
        }
        
        $saveResult = $this->saveFileWithoutLock($file);
        if(!$saveResult)
        {
            $filePath = $this->getFilePath();
            $error = 'Impossible to save the session to '. $filePath;
            $errors[] = $error;
            return $errors;
        }
        return $errors;
    }
    
    protected function
        saveFileReturnSuccess($file) : bool
    {
        return empty($this->saveFile($file));
    }
    
    protected function
        saveFileAndUnlock($file) : array
    {
        $result = $this->saveFile($file);
        $this->unlockFile();
        return $result;
    }
    
    protected function
        saveFileAndUnlockReturnSuccess($file) : bool
    {
        return empty($this->saveFileAndUnlock($file));
    }
    
    protected function
        loadFile($file) : array
    {
        $errors = $this->lockFile();
        if($this->isForcingLock() && !empty($errors))
        {
            $filePath = $this->getFilePath();
            $error = 'Impossible to lock the session for '. $filePath;
            $errors[] = $error;
            return $errors;
        }
        
        $loadResult = $this->loadFileWithoutUnlock($file);
        if(!$loadResult)
        {
            $this->unlockFile();
            $filePath = $this->getFilePath();
            $error = 'Impossible to load the file '. $filePath;
            $errors[] = $error;
            return $errors;
        }
        return $errors;
    }
    
    protected function
        loadFileReturnSuccess($file) : bool
    {
        return empty($this->loadFile($file));
    }
    
    protected function
        loadFileIfExist($file) : array
    {
        if(!file_exists($this->getFilePath()))
        {
            return array('File '. $this->getFilePath() .' does not exist');
        }
        return $this->loadFile($file);
    }
    
    protected function
        loadFileIfExistReturnSuccess($file) : bool
    {
        return empty($this->loadFileIfExist($file));
    }
}
