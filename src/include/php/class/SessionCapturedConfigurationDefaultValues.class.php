<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class SessionCapturedConfigurationDefaultValues
    extends SessionCapturedConfigurationAbstract
{
    const DIRECTORY         = ABSOLUTE_PATH .'data';
    const FILE_NAME_PREFIX  = 'session';
    const FILE_EXTENSION    = '.xml';
    
    const SAVE              = OptionnalBooleanConst::TRUE;
    const CREATE_AUTO       = OptionnalBooleanConst::TRUE;
    const SAVE_PROXY        = OptionnalBooleanConst::TRUE;
    
    
    public function
        getDirectory() : string
    {
        return self::DIRECTORY;
    }
    
    public function
        getFileName() : string
    {
        if(SessionIdentifierUtils::isInitialized())
        {
            return (
                self::FILE_NAME_PREFIX .'-'.
                SessionIdentifierUtils::get()
                . self::FILE_EXTENSION
            );
        }
        return self::FILE_NAME_PREFIX . self::FILE_EXTENSION;
    }
    
    
    public function
        hasToSave() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_PROXY);
    }
    
    public function
        hasToCreateAutomatically() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::CREATE_AUTO);
    }
    
    public function
        hasToSaveProxy() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::SAVE_PROXY);
    }
    
    public function
        getHttp() : HttpSessionCapturedConfigurationAbstract
    {
        return new HttpSessionCapturedConfigurationDefaultValues();
    }
}
