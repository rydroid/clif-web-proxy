<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @see http://www.cplusplus.com/reference/utility/pair/
 */
final class Pair
{
    public $first;
    public $second;
    
    public function
        __construct()
    {
        $this->first  = null;
        $this->second = null;
    }
    
    public static function
        create($first, $second) : Pair
    {
        $pair = new Pair();
        $pair->first  = $first;
        $pair->second = $second;
        return $pair;
    }
    
    public function
        swap()
    {
        $tmp = $this->second;
        $this->second = $this->first;
        $this->first  = $tmp;
    }
    
    public function
        isEmpty() : bool
    {
        return $this->first == null && $this->second == null;
    }
}
