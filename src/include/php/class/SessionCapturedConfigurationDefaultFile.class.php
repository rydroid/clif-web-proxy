<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


define(
    'SESSION_CAPTURED_CONFIGURATION_DEFAULT_FILE_PATH',
    ABSOLUTE_PATH . 'configuration/session-captured.ini'
);


final class SessionCapturedConfigurationDefaultFile
{
    private static $conf = null;
    
    
    public static function
        loadFromPathForce() : bool
    {
        self::$conf = SessionCapturedConfigurationUtils::createFromFilePath(
            SESSION_CAPTURED_CONFIGURATION_DEFAULT_FILE_PATH
        );
        return self::$conf != null;
    }
    
    public static function
        loadFromPath() : bool
    {
        if(self::$conf == null)
        {
            return self::loadFromPathForce();
        }
        return true;
    }
    
    public static function
        get() : SessionCapturedConfigurationAbstract
    {
        self::loadFromPath();
        if(self::$conf == null)
        {
            return new SessionCapturedConfigurationNull();
        }
        return self::$conf;
    }
}
