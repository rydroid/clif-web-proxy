<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @see https://bugs.php.net/bug.php?id=49437
 */
class DomStringPreparatorEntity
    extends DomStringPreparatorAbstract
{
    public function
        prepare(string $domString) : string
    {
        $result = $domString;
        $result = str_replace('&nbsp;',   '@nbsp;',   $result);
        $result = str_replace('&middot;', '@middot;', $result);
        $result = str_replace('&bullet;', '@bullet;', $result);
        $result = str_replace('&egrave;', '@egrave;', $result);
        $result = str_replace('&eacute;', '@eacute;', $result);
        $result = str_replace('&agrave;', '@agrave;', $result);
        $result = str_replace('&ugrave;', '@ugrave;', $result);
        $result = str_replace('&ccedil;', '@ccedil;', $result);
        $result = str_replace('&ocirc;',  '@ocirc;', $result);
        return $result;
    }
    
    public function
        unprepare(string $domString) : string
    {
        $result = $domString;
        $result = str_replace('@ocirc;',  '&ocirc;', $result);
        $result = str_replace('@ccedil;', '&ccedil;', $result);
        $result = str_replace('@ugrave;', '&ugrave;', $result);
        $result = str_replace('@agrave;', '&agrave;', $result);
        $result = str_replace('@eacute;', '&eacute;', $result);
        $result = str_replace('@egrave;', '&egrave;', $result);
        $result = str_replace('@bullet;', '&bullet;', $result);
        $result = str_replace('@middot;', '&middot;', $result);
        $result = str_replace('@nbsp;',   '&nbsp;',   $result);
        return $result;
    }
}
