<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class HttpHeadersUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const SERVER_SPECIFIC_KEYS = ['Host', 'Accept', 'Accept-Encoding'];
    
    
    public static function
        createClone(HttpHeaders $headers) : HttpHeaders
    {
        return HttpHeaders::createFromArrayOfLines(
            $headers->getArrayOfLines()
        );
    }
    
    public static function
        send(HttpHeaders $headers)
    {
        foreach($headers->getArrayOfLines() as $line)
        {
            if(trim($line) != '')
            {
                header($line);
            }
        }
    }
    
    public static function
        removeServerSpecificKeys(HttpHeaders& $headers)
    {
        foreach(self::SERVER_SPECIFIC_KEYS as $key)
        {
            $headers->removeKey($key);
        }
    }
    
    public static function
        toDomElement(HttpHeaders $headers, DOMDocument $document,
                     string $from = '')
        : DOMElement
    {
        $headersElement = $document->createElement('headers');
        if(!empty(trim($from)))
        {
            $headersElement->setAttribute('from', $from);
        }
        foreach($headers->getArrayOfHttpHeaderLine() as $headerLine)
        {
            if(!$headerLine->isNull())
            {
                $headerElement = HttpHeaderLineUtils::toDomElement(
                    $headerLine, $document
                );
                $headersElement->appendChild($headerElement);
            }
        }
        return $headersElement;
    }
}
