<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class UserInterfaceConfigurationDefaultValues
    extends UserInterfaceConfigurationAbstract
{
    const DEBUG          = OptionnalBooleanConst::FALSE;
    const PROXY_GET      = OptionnalBooleanConst::FALSE;
    const PROXY_COOKIES  = OptionnalBooleanConst::TRUE;
    const CLIENT_SCRIPTS = OptionnalBooleanConst::TRUE;
    
    
    public function
        __construct()
    {}
    
    
    public function
        showDebug() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::DEBUG);
    }
    
    public function
        showProxyGetParametersConfiguration() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::PROXY_GET);
    }
    
    public function
        showProxyCookiesConfiguration() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::PROXY_COOKIES);
    }
    
    public function
        useClientScripts() : OptionnalBoolean
    {
        return new OptionnalBoolean(self::CLIENT_SCRIPTS);
    }
}
