<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class LogsConfigurationPropertyConst
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const SAVE_KEYS_WITHOUT_SECTION = [
        'logs-save', 'log-save', 'save-logs', 'save-log'
    ];
    const FILE_PATH_KEYS_WITHOUT_SECTION = [
        'logs-file-path', 'log-file-path',
        'logs-filepath',  'log-filepath'
    ];
    const FILE_NAME_KEYS_WITHOUT_SECTION = [
        'logs-file-name', 'log-file-name',
        'logs-filename',  'log-filename',
        'logfilename'
    ];
    const DIRECTORY_KEYS_WITHOUT_SECTION = [
        'logs-directory', 'log-directory', 'logs-folder', 'log-folder'
    ];
    const SHOW_KEYS_WITHOUT_SECTION = [
        'logs-show', 'log-show', 'show-logs', 'show-log'
    ];
}
