<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UserInterfaceConfigurationPrinterUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        printAsHtmlList(UserInterfaceConfigurationAbstract $configuration,
                        string $linePrefix = '')
    {
        echo $linePrefix .'<ul>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Show debug: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->showDebug()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>';
        echo 'Show configuration of proxy with GET parameters: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->showProxyGetParametersConfiguration()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>';
        echo 'Show configuration of proxy with cookies: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->showProxyCookiesConfiguration()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Use client scripts (like JavaScript): ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->useClientScripts()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix .'</ul>'. PHP_EOL;
    }
}
