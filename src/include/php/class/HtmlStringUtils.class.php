<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HtmlStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getCharsetFromXml(string $aString) : string
    {
        $document = new DOMDocument();
        $document->preserveWhiteSpace = false;
        if(!@$document->loadXML($aString))
        {
            return '';
        }
        return HtmlDocumentUtils::getCharset($document);
    }
    
    public static function
        getCharsetFromHtml(string $aString) : string
    {
        $document = new DOMDocument();
        $document->preserveWhiteSpace = false;
        if(!@$document->loadHTML($aString))
        {
            return '';
        }
        return HtmlDocumentUtils::getCharset($document);
    }
    
    public static function
        getCharset(string $aString) : string
    {
        $aStringTrimmed = trim($aString);
        if($aStringTrimmed == '')
        {
            return '';
        }
        
        $charset = self::getCharsetFromXml($aStringTrimmed);
        if($charset == '')
        {
            $charset = self::getCharsetFromHtml($aStringTrimmed);
        }
        return $charset;
    }
    
    public static function
        hasCharset(string $aString) : bool
    {
        $charset = self::getCharset($aString);
        $charset = trim($charset);
        return $charset != '';
    }
}
