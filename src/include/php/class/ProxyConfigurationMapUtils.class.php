<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxyConfigurationMapUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getKeyOfUrl(array $aMap) : string
    {
        foreach(ProxyConfigurationConst::KEYS_FOR_URL as $key)
        {
            if(isset($aMap[$key]) &&
               !empty(trim($aMap[$key])))
            {
                return $key;
            }
        }
        return '';
    }
    
    public static function
        getUrl(array $aMap, string $defaultValue = '') : string
    {
        $key = self::getKeyOfUrl($aMap);
        if(empty($key))
        {
            return $defaultValue;
        }
        return $aMap[$key];
    }
    
    public static function
        getKeyOfPort(array $aMap) : string
    {
        foreach(ProxyConfigurationConst::KEYS_FOR_PORT as $key)
        {
            if(isset($aMap[$key]) &&
               !empty(trim($aMap[$key])) &&
               is_numeric($aMap[$key]))
            {
                return $key;
            }
        }
        return '';
    }
    
    public static function
        getPort(array $aMap, int $defaultValue = -1) : int
    {
        $key = self::getKeyOfPort($aMap);
        if(empty($key))
        {
            return $defaultValue;
        }
        return (int) $aMap[$key];
    }
}
