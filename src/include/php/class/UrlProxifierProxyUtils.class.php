<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UrlProxifierProxyUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        getParametersAsMap() : array
    {
        $key = ProxyConfigurationGetParametersUtils::getKeyForUrl();
        if($key == '')
        {
            return array();
        }
        
        $parameters = array(
            ProxyConfigurationConst::DEFAULT_KEY_FOR_URL => $_GET[$key]
        );
        $key = ProxyConfigurationGetParametersUtils::getKeyForPort();
        if($key != '')
        {
            $parameters[ProxyConfigurationConst::DEFAULT_KEY_FOR_PORT] = (
                $_GET[$key]
            );
        }
        return $parameters;
    }
    
    public static function
        getEncodedParametersAsMap() : array
    {
        $parameters = self::getParametersAsMap();
        if(isset($parameters[ProxyConfigurationConst::DEFAULT_KEY_FOR_URL]))
        {
            # TODO fix this hack (should not need to encode 2 times)
            UrlProxifierCodecUtils::encodeUrl(
                $parameters[ProxyConfigurationConst::DEFAULT_KEY_FOR_URL]
            );
            UrlProxifierCodecUtils::encodeUrl(
                $parameters[ProxyConfigurationConst::DEFAULT_KEY_FOR_URL]
            );
        }
        return $parameters;
    }
    
    public static function
        getParametersAsString() : string
    {
        $map = self::getParametersAsMap();
        $result = GetParametersUtils::mapToString($map);
        return $result;
    }
    
    public static function
        getEncodedParametersAsString() : string
    {
        $map = self::getEncodedParametersAsMap();
        $result = GetParametersUtils::mapToString($map);
        return $result;
    }
    
    public static function
        addParametersToUrl(string $url) : string
    {
        $parameters = self::getParametersAsString();
        $newUrl = UrlStringUtils::addStringParameters($url, $parameters);
        return $newUrl;
    }
    
    public static function
        removeParametersOfUrl(string $url) : string
    {
        $parameters = self::getParametersAsString();
        $newUrl = UrlStringUtils::removeStringParameters($url, $parameters);
        return $newUrl;
    }
    
    public static function
        removeEncodedParametersOfUrl(string $url) : string
    {
        $parametersString = self::getEncodedParametersAsString();
        $newUrl = UrlStringUtils::removeStringParameters(
            $url, $parametersString
        );
        return $newUrl;
    }
}
