<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class BooleanStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static function
        prepare(string $aString) : string
    {
        return strtolower(trim($aString));
    }
    
    private static function
        isFalsePrepared(string $aString) : bool
    {
        return $aString == 'false' || $aString == 'faux';
    }
    
    private static function
        isFalseExtendedPrepared(string $aString) : bool
    {
        return (
            self::isFalsePrepared($aString) ||
            $aString == 'no' ||
            $aString == 'off' ||
            $aString == 'non'
        );
    }
    
    public static function
        isFalse(string $aString) : bool
    {
        $stringPrepared = self::prepare($aString);
        return self::isFalsePrepared($stringPrepared);
    }
    
    public static function
        isFalseExtended(string $aString) : bool
    {
        $stringPrepared = self::prepare($aString);
        return self::isFalseExtendedPrepared($stringPrepared);
    }
    
    
    private static function
        isTruePrepared(string $aString) : bool
    {
        return $aString == 'true' || $aString == 'vrai';
    }
    
    private static function
        isTrueExtendedPrepared(string $aString) : bool
    {
        return (
            self::isTruePrepared($aString) ||
            $aString == 'yes' ||
            $aString == 'on' ||
            $aString == 'oui'
        );
    }
    
    public static function
        isTrue(string $aString) : bool
    {
        $stringPrepared = self::prepare($aString);
        return self::isTruePrepared($stringPrepared);
    }
    
    public static function
        isTrueExtended(string $aString) : bool
    {
        $stringPrepared = self::prepare($aString);
        return self::isTrueExtendedPrepared($stringPrepared);
    }
    
    
    public static function
        isBoolean(string $aString) : bool
    {
        $stringPrepared = self::prepare($aString);
        return (
            self::isFalsePrepared($stringPrepared) ||
            self::isTruePrepared($stringPrepared)
        );
    }
    
    public static function
        isBooleanExtended(string $aString) : bool
    {
        $stringPrepared = self::prepare($aString);
        return (
            self::isFalseExtendedPrepared($stringPrepared) ||
            self::isTrueExtendedPrepared($stringPrepared)
        );
    }
    
    
    public static function
        toYesNo(bool $value) : string
    {
        return $value ? 'yes' : 'no';
    }
}
