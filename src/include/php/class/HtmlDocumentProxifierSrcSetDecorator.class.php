<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierSrcSetDecorator
    extends DomDocumentProxifierDecorator
{
    private $xpathQuery;
    
    
    public function
        __construct(string $xpathQuery,
                    DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
        $this->xpathQuery = $xpathQuery;
    }
    
    
    public function
        getXPathQuery() : string
    {
        return $this->xpathQuery .'[string(@srcset)]';
    }
    
    public function
        getElements(DOMDocument $document) : DOMNodeList
    {
        $docXPath = new DOMXPath($document);
        $queryString = $this->getXPathQuery();
        $elements = $docXPath->query($queryString);
        return $elements;
    }
    
    /**
     * @return Number of modified elements, or -1 if a problem occured
     */
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        
        $elements = $this->getElements($document);
        $nbModified += HtmlDocumentProxifierSrcSetUtils::proxifyNodeList(
            $this, $elements
        );
        
        return $nbModified;
    }
}
