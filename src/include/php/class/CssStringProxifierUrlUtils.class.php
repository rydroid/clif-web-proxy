<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class CssStringProxifierUrlUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const FUNCTION_NAME = 'url';
    
    
    public static function
        isImportBefore(string $line, int $positionStart, int $positionBefore)
        : bool
    {
        $lineStartLength = $positionBefore - $positionStart;
        $lineStart = substr($line, $positionStart, $lineStartLength);
        $lineStartAscii = preg_replace(
            '/[\x00-\x1F\x7F-\xFF]/', '', $lineStart
        );
        $lineStartTrimmed = trim($lineStartAscii);
        return $lineStartTrimmed == '@import';
    }
    
    public static function
        hasToProxifyConsideringWhatIsBeforeNoImportNoHexa(string $line,
                                                          int $positionBefore)
        : bool
    {
        while(true)
        {
            if($positionBefore < 0)
            {
                return false;
            }
            
            $currentCharacter = $line[$positionBefore];
            if($currentCharacter == ':' ||
               $currentCharacter == ',')
            {
                return true;
            }
            if($currentCharacter == ' ' ||
               $currentCharacter == '	' ||
               ctype_alnum($currentCharacter))
            {
                --$positionBefore;
                continue;
            }
            return false;
        }
    }
    
    public static function
        endsWith3HexColor(string $line, int $positionBefore) : bool
    {
        if(ctype_xdigit($line[$positionBefore]) &&
           $line[$positionBefore - 3] == '#')
        {
            $potentialColor = substr($line, $positionBefore - 2, 3);
            return StringUtils::isHexadecimal($potentialColor);
        }
        return false;
    }
    
    public static function
        endsWith6HexColor(string $line, int $positionBefore) : bool
    {
        if(ctype_xdigit($line[$positionBefore]) &&
           $line[$positionBefore - 6] == '#')
        {
            $potentialColor = substr($line, $positionBefore - 5, 6);
            return StringUtils::isHexadecimal($potentialColor);
        }
        return false;
    }

    public static function
        hasToProxifyConsideringWhatIsBeforeNoImport(string $line,
                                                    int $positionBefore)
        : bool
    {
        while($positionBefore > 6)
        {
            if(self::endsWith3HexColor($line, $positionBefore))
            {
                return self::hasToProxifyConsideringWhatIsBeforeNoImportNoHexa(
                    $line, $positionBefore - 4
                );
            }
            if(self::endsWith6HexColor($line, $positionBefore))
            {
                return self::hasToProxifyConsideringWhatIsBeforeNoImportNoHexa(
                    $line, $positionBefore - 7
                );
            }
            
            $currentCharacter = $line[$positionBefore];
            if($currentCharacter == ':' ||
               $currentCharacter == ',')
            {
                return true;
            }
            if($currentCharacter == ' ' ||
               $currentCharacter == '	' ||
               ctype_alnum($currentCharacter))
            {
                --$positionBefore;
                continue;
            }
            return false;
        }
        return self::hasToProxifyConsideringWhatIsBeforeNoImportNoHexa(
            $line, $positionBefore
        );
    }
    
    public static function
        hasToProxifyConsideringWhatIsBefore(string $line,
                                            int $positionStart,
                                            int $positionBefore)
        : bool
    {
        if($positionBefore < 0)
        {
            return false;
        }
        
        if(self::isImportBefore($line, $positionStart, $positionBefore))
        {
            return true;
        }
        return self::hasToProxifyConsideringWhatIsBeforeNoImport(
            $line, $positionBefore - 1
        );
    }
    
    public static function
        findDelimiterEndPositionOfUrlArgument(string $argument,
                                              int $positionEnd,
                                              string $delimiter)
        : int
    {
        $delimiterEnd = $positionEnd;
        while(true)
        {
            $currentCharacter = $argument[$delimiterEnd-1];
            if($currentCharacter == ' ' || $currentCharacter == '	')
            {
                --$delimiterEnd;
                continue;
            }
            if($currentCharacter == $delimiter)
            {
                --$delimiterEnd;
            }
            break;
        }
        return $delimiterEnd;
    }
    
    public static function
        findEndPosition(string $line, int $offset) : int
    {
        $positionEnd = strpos($line, ',', $offset);
        if($positionEnd !== FALSE)
        {
            return $positionEnd;
        }
        
        $positionEnd = strpos($line, ';', $offset);
        if($positionEnd !== FALSE)
        {
            return $positionEnd;
        }
        
        $positionEnd = strpos($line, '}', $offset);
        if($positionEnd !== FALSE)
        {
            return $positionEnd;
        }
        
        return -1;
    }
}
