<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class SessionCapturedConfigurationPrinterUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getFilePathAsHtml(SessionCapturedConfigurationAbstract $configuration)
        : string
    {
        if($configuration->hasFilePath())
        {
            return $configuration->getFilePath();
        }
        return '<span class="red">none</span>';
    }
    
    public static function
        printAsHtmlList(SessionCapturedConfigurationAbstract $configuration,
                        string $linePrefix = '')
    {
        echo $linePrefix .'<ul>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSave()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Create automatically: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToCreateAutomatically()
        );
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>File path: ';
        echo self::getFilePathAsHtml($configuration);
        echo '</li>'. PHP_EOL;
        echo $linePrefix ."\t". '<li>Save proxy: ';
        echo OptionnalBooleanUtils::toYesNoUndefined(
            $configuration->hasToSaveProxy()
        );
        HttpSessionCapturedConfigurationPrinterUtils::printAsHtmlListItems(
            $configuration->getHttp(), $linePrefix
        );
        echo $linePrefix .'</ul>'. PHP_EOL;
    }
}
