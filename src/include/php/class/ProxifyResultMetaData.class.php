<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class ProxifyResultMetaData
{
    private $url;
    private $timeRequestMicroSecond;
    private $timeResponseMicroSecond;
    private $headersClient;
    
    
    public function
        __construct(string $url,
                    float $timeRequestMicroSec  = -1,
                    float $timeResponseMicroSec = -1)
    {
        $this->url = UrlProxifierProxyUtils::removeEncodedParametersOfUrl($url);
        $this->timeRequestMicroSecond  = $timeRequestMicroSec;
        $this->timeResponseMicroSecond = $timeResponseMicroSec;
        $this->headersClient = null;
    }
    
    
    public function
        getUrl() : string
    {
        return $this->url;
    }
    
    public function
        hasUrl() : bool
    {
        return !empty(trim($this->url));
    }
    
    
    public function
        getTimeRequestMicroSecond() : float
    {
        return $this->timeRequestMicroSecond;
    }
    
    public function
        getTimeResponseMicroSecond() : float
    {
        return $this->timeResponseMicroSecond;
    }
    
    public function
        getTimeRequestMilliSecond() : float
    {
        return $this->getTimeRequestMicroSecond() / 1000;
    }
    
    public function
        getTimeResponseMilliSecond() : float
    {
        return $this->getTimeResponseMicroSecond() / 1000;
    }
    
    public function
        isValidTimeRequest() : bool
    {
        return $this->timeRequestMicroSecond > 0;
    }
    
    public function
        isValidTimeResponse() : bool
    {
        return $this->timeResponseMicroSecond > 0;
    }
    
    public function
        setTimeRequestMicroSecond(float $value) : bool
    {
        if($value < 0)
        {
            return false;
        }
        $this->timeRequestMicroSecond = $value;
        return true;
    }
    
    public function
        setTimeResponseMicroSecond(float $value) : bool
    {
        if($value < 0)
        {
            return false;
        }
        $this->timeResponseMicroSecond = $value;
        return true;
    }
    
    public function
        setTimeRequestMilliSecond(float $value) : bool
    {
        return $this->setTimeRequestMicroSecond($value * 1000);
    }
    
    public function
        setTimeResponseMilliSecond(float $value) : bool
    {
        return $this->setTimeResponseMicroSecond($value * 1000);
    }
    
    public function
        setTimeRequestToNow() : bool
    {
        return $this->setTimeRequestMilliSecond(microtime(true) * 1000);
    }
    
    public function
        setTimeResponseToNow() : bool
    {
        return $this->setTimeResponseMilliSecond(microtime(true) * 1000);
    }
    
    
    public function
        hasHeadersClient() : bool
    {
        return $this->headersClient != null && !$this->headersClient->isEmpty();
    }
    
    public function
        setHeadersClient(HttpHeaders $headers) : bool
    {
        if($headers->isEmpty())
        {
            return false;
        }
        $this->headersClient = $headers;
        return true;
    }
    
    public function
        getHeadersClientClone() : HttpHeaders
    {
        if($this->headersClient == null)
        {
            return HttpHeaders::createEmpty();
        }
        return HttpHeadersUtils::createClone($this->headersClient);
    }
}
