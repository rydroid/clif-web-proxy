<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierJavaScriptDynamic
    extends DomDocumentProxifierDecorator
{
    const SCRIPT_FILE_PATH = 'include/js/specific/javascript-proxifier.js';
    const SCRIPT_MIME_TYPE = 'text/javascript';
    
    
    public function
        __construct(DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
    }
    
    
    public static function
        getNbScripts(DOMDocument $document) : int
    {
        $documentXPath = new DOMXPath($document);
        return $documentXPath->query('/html/body//script')->length;
    }
    
    public static function
        getNbScriptsNonNull(DOMDocument $document) : int
    {
        $documentXPath = new DOMXPath($document);
        $scriptElementList = $documentXPath->query('/html/body//script');
        $nbScripts = 0;
        foreach($scriptElementList as $scriptElement)
        {
            if(!empty(trim($scriptElement->nodeValue)) ||
               ($scriptElement->hasAttribute('src') &&
                !empty(trim($scriptElement->getAttribute('src')))))
            {
                ++$nbScripts;
            }
        }
        return $nbScripts;
    }
    
    public static function
        hasAtLeastOneScript(DOMDocument $document) : bool
    {
        return self::getNbScriptsNonNull($document) > 0;
    }
    
    public function
        addScript(DOMDocument $document) : bool
    {
        $htmlElement = $document->documentElement;
        if($htmlElement == NULL ||
           strtolower($htmlElement->nodeName) != 'html')
        {
            return false;
        }
        
        foreach($htmlElement->childNodes as $potentialBodyElement)
        {
            if(strtolower($potentialBodyElement->nodeName) == 'body')
            {
                $scriptElement = $document->createElement('script');
                $scriptElement->setAttribute('src',  self::SCRIPT_FILE_PATH);
                $scriptElement->setAttribute('type', self::SCRIPT_MIME_TYPE);
                // Add option to add attribute async
                // https://www.w3schools.com/tags/att_script_async.asp
                $potentialBodyElement->appendChild($scriptElement);
                return true;
            }
        }
        return false;
    }
    
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        
        if(self::hasAtLeastOneScript($document))
        {
            if($this->addScript($document))
            {
                ++$nbModified;
            }
        }
        
        return $nbModified;
    }
}
