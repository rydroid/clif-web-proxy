<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class StringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        contains(string $haystack, string $needle) : bool
    {
        return strpos($haystack, $needle) !== FALSE;
    }
    
    public static function
        startsWith(string $haystack, string $needle) : bool
    {
        $length = strlen($needle);
        return substr($haystack, 0, $length) === $needle;
    }
    
    public static function
        startsWithInsensitive(string $haystack, string $needle) : bool
    {
        $length = strlen($needle);
        $needle = mb_strtolower($needle);
        return mb_strtolower(substr($haystack, 0, $length)) === $needle;
    }
    
    public static function
        endsWith(string $haystack, string $needle) : bool
    {
        $length = strlen($needle);
        if($length == 0)
        {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }
    
    public static function
        endsWithInsensitive(string $haystack, string $needle) : bool
    {
        return self::endsWith(mb_strtolower($haystack), mb_strtolower($needle));
    }
    
    public static function
        endsWithArray(string $haystack, array $needles) : bool
    {
        foreach($needles as $needle)
        {
            if(self::endsWith($haystack, $needle))
            {
                return true;
            }
        }
        return false;
    }
    
    public static function
        endsWithInsensitiveArray(string $haystack, array $needles) : bool
    {
        foreach($needles as $needle)
        {
            if(self::endsWithInsensitive($haystack, $needle))
            {
                return true;
            }
        }
        return false;
    }
    
    public static function
        isHexadecimal(string $aString) : bool
    {
        $length = strlen($aString);
        for($index = 0; $index < $length; ++$index)
        {
            if(!ctype_xdigit($aString[$index]))
            {
                return false;
            }
        }
        return true;
    }
}
