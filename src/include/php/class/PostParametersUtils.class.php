<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class PostParametersUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public static function
        get() : array
    {
        if(isset($_POST))
        {
            return $_POST;
        }
        return array();
    }
    
    public static function
        isEmpty()
    {
        return empty(self::get());
    }
    
    public static function
        mapToString(array $aMap) : string
    {
        $result = GetParametersUtils::mapToString($aMap);
        return $result;
    }
    
    
    private static function
        fieldToDomElement(DOMDocument $document, string $key, string $value)
        : DOMElement
    {
        $fieldElement = $document->createElement('field');
        
        $keyElement = $document->createElement('key');
        $keyElement->nodeValue = $key;
        $fieldElement->appendChild($keyElement);
        
        $valueElement = $document->createElement('value');
        $valueElement->nodeValue = $value;
        $fieldElement->appendChild($valueElement);
        
        return $fieldElement;
    }
    
    public static function
        toDomElementFromArray(DOMDocument $document, array $anArray)
        : DOMElement
    {
        $postFieldsElement = $document->createElement('postFields');
        foreach($anArray as $key => $value)
        {
            $fieldElement = self::fieldToDomElement($document, $key, $value);
            $postFieldsElement->appendChild($fieldElement);
        }
        return $postFieldsElement;
    }
    
    public static function
        toDomElement(DOMDocument $document) : DOMElement
    {
        return self::toDomElementFromArray($document, self::get());
    }
}
