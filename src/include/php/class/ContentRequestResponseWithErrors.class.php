<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class ContentRequestResponseWithErrors
    extends ContentRequestResponse
{
    private $errors = [];
    
    public function
        getErrors() : array
    {
        return $this->errors;
    }
    
    public function
        countErrors() : int
    {
        return count($this->errors);
    }
    
    public function
        addError(string $error) : bool
    {
        if(empty(trim($error)) || in_array($error, $this->errors))
        {
            return false;
        }
        $this->errors[] = $error;
        return true;
    }
    
    public function
        addErrors(array $errors) : int
    {
        $nbErrors = 0;
        foreach($errors as $error)
        {
            if($this->addError($error))
            {
                ++$nbErrors;
            }
        }
        return $nbErrors;
    }
}
