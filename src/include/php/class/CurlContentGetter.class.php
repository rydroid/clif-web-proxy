<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class CurlContentGetter
    extends AbstractContentGetter
{
    const GET_HEADER_DEFAULT = TRUE;
    
    public static function
        canWork()
    {
        return function_exists('curl_version');
    }
    
    public static function
        addErrorString(ContentRequestResponseWithErrors $requestResponse,
                       string $error)
        : bool
    {
        if(empty(trim($error)))
        {
            return false;
        }
        return $requestResponse->addError('curl error: '. $error);
    }
    
    public static function
        addErrorCurl(ContentRequestResponseWithErrors $requestResponse,
                     $curlHandler)
        : bool
    {
        $error = curl_error($curlHandler);
        if(!is_string($error))
        {
            return false;
        }
        return self::addErrorString($requestResponse, $error);
    }
    
    private static function
        setProxyToHandler($curlHandler) : bool
    {
        if(ProxyConfigurationGeneral::isNull())
        {
            return false;
        }
        
        $url = ProxyConfigurationGeneral::getUrl();
        if($url != '')
        {
            curl_setopt(
                $curlHandler, CURLOPT_HTTPPROXYTUNNEL,
                UrlProtocolStringUtils::isWeb($url)
            );
            curl_setopt($curlHandler, CURLOPT_PROXY, $url);
        }
        if(ProxyConfigurationGeneral::hasPort())
        {
            curl_setopt(
                $curlHandler, CURLOPT_PROXYPORT,
                ProxyConfigurationGeneral::getPort()
            );
        }
        return true;
    }
    
    private static function
        setPostParametersToHandlerFromArray($curlHandler, array $anArray)
        : bool
    {
        if(isset($anArray) && !empty($anArray))
        {
            $postParamsString = PostParametersUtils::mapToString($anArray);
            if(!empty($postParamsString))
            {
                curl_setopt($curlHandler, CURLOPT_POST, TRUE);
                curl_setopt(
                    $curlHandler, CURLOPT_POSTFIELDS,
                    $postParamsString
                );
                return true;
            }
        }
        return false;
    }
    
    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    private static function
        setPostParametersToHandler($curlHandler) : bool
    {
        return self::setPostParametersToHandlerFromArray($curlHandler, $_POST);
    }
    
    
    private $getHeader;
    
    
    public function
        __construct(bool $getHeader = self::GET_HEADER_DEFAULT)
    {
        $this->getHeader = $getHeader;
    }
    
    
    private function
        prepare()
    {
        $curlHandler = curl_init();
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curlHandler, CURLOPT_HEADER, $this->getHeader);
        curl_setopt($curlHandler, CURLOPT_POST,
                    strtoupper(ServerUtils::getRequestMethod() == 'POST'));
        self::setProxyToHandler($curlHandler);
        self::setPostParametersToHandler($curlHandler);
        return $curlHandler;
    }
    
    private function
        getWithHandler($curlHandler, string $url) : ContentRequestResponse
    {
        curl_setopt($curlHandler, CURLOPT_URL, $url);
        $response = curl_exec($curlHandler);
        
        if($response === FALSE)
        {
            $result = new HttpRequestResponse('');
            self::addErrorCurl($result, $curlHandler);
            return $result;
        }
        
        if($this->getHeader)
        {
            $headerSize = curl_getinfo($curlHandler, CURLINFO_HEADER_SIZE);
            $content = substr($response, $headerSize);
            $charset = HtmlStringUtils::getCharset($content);
            if(FileContentUtils::seemToBeHtml($content) &&
               !CharsetStringUtils::isUtf8($charset))
            {
                $content = utf8_decode($content);
            }
            $header  = substr($response, 0, $headerSize);
            return new HttpRequestResponse($content, $header);
        }
        return new HttpRequestResponse($response);
    }
    
    private function
        getWithHandlerClever($curlHandler, string $url)
        : ContentRequestResponse
    {
        if(StringUtils::startsWith($url, '//'))
        {
            $response = $this->getWithHandler(
                $curlHandler, 'https:'. $url
            );
            if($response->getContent() == '')
            {
                $response = $this->getWithHandler(
                    $curlHandler, 'http:'. $url
                );
            }
            return $response;
        }
        $response = $this->getWithHandler($curlHandler, $url);
        return $response;
    }
    
    public function
        get(string $url, HttpHeaders $headers) : ContentRequestResponse
    {
        $curlHandler = $this->prepare();
        if(!$headers->isEmpty())
        {
            $headers->removeKey('Content-Length');
            if(!$headers->isEmpty())
            {
                curl_setopt(
                    $curlHandler, CURLOPT_HTTPHEADER,
                    $headers->getArrayOfLines()
                );
            }
        }
        
        $response = $this->getWithHandlerClever($curlHandler, $url);
        if(curl_errno($curlHandler))
        {
            self::addErrorCurl($response, $curlHandler);
        }
        curl_close($curlHandler);
        return $response;
    }
}
