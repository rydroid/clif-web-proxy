<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class HtmlDocumentProxifierUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    /**
     * @return Number of modified elements, or -1 if a problem occured
     */
    public static function
        proxifyDocument(DOMDocument $document) : int
    {
        $proxifier = new HtmlDocumentProxifier();
        return $proxifier->proxifyDocument($document);
    }
    
    public static function
        proxifyDocumentToString(DOMDocument $document) : string
    {
        self::proxifyHtmlDocument($document);
        return $document->saveHTML();
    }
    
    public static function
        proxifyString(string $domString) : string
    {
        $proxifier = new HtmlDocumentProxifier();
        return $proxifier->proxifyHtmlString($domString);
    }
}
