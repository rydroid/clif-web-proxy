<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @SuppressWarnings(PHPMD.Superglobals)
 */
final class ProxyConfigurationCookiesUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getUrl() : string
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return '';
        }
        return ProxyConfigurationMapUtils::getUrl($_COOKIE);
    }
    
    public static function
        hasUrl() : bool
    {
        return !empty(trim(self::getUrl()));
    }
    
    
    public static function
        getPort() : int
    {
        if(!isset($_COOKIE) || empty($_COOKIE))
        {
            return -1;
        }
        return ProxyConfigurationMapUtils::getPort($_COOKIE);
    }
    
    public static function
        hasPort() : bool
    {
        return self::getPort() >= 0;
    }
}
