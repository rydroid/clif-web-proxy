<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxifierGetParametersUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const KEYS_TO_PROXIFY = ['url-to-proxify', 'url', 'URL', 'q'];
    const DEFAULT_KEY_TO_PROXIFY = self::KEYS_TO_PROXIFY[0];
    
    
    public static function
        getKeyOfUrlToProxifyFromMap(array $aMap) : string
    {
        foreach(self::KEYS_TO_PROXIFY as $key)
        {
            if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, $key))
            {
                return $key;
            }
        }
        return '';
    }
    
    public static function
        getUrlToProxifyFromMap(array $aMap) : string
    {
        $key = self::getKeyOfUrlToProxifyFromMap($aMap);
        if(empty($key))
        {
            return '';
        }
        return $aMap[$key];
    }
    
    public static function
        getArrayParamsOfProxifiedUrlFromArray(array $anArray) : array
    {
        $result = $anArray;
        $keyToRemove = self::getKeyOfUrlToProxifyFromMap($anArray);
        ArrayUtils::removeKey($result, $keyToRemove);
        return $result;
    }
    
    public static function
        getStrOfParamsOfProxifiedUrlFromMap(array $anArray) : string
    {
        $resultArray = self::getArrayParamsOfProxifiedUrlFromArray($anArray);
        $resultString = GetParametersUtils::mapToString($resultArray);
        return $resultString;
    }
    
    public static function
        getStrOfParamsOfProxifiedUrlFromMapToEncode(array $anArray) : string
    {
        $resultArray = self::getArrayParamsOfProxifiedUrlFromArray($anArray);
        $resultString = GetParametersUtils::mapNotEncodedToString($resultArray);
        return $resultString;
    }
    
    public static function
        getUrlToProxifyWithParamsFromArray(array $anArray) : string
    {
        $url = self::getUrlToProxifyFromMap($anArray);
        $parametersString = self::getStrOfParamsOfProxifiedUrlFromMapToEncode(
            $anArray
        );
        if(empty($parametersString))
        {
            return $url;
        }
        $url .= '?'. $parametersString;
        return $url;
    }
}
