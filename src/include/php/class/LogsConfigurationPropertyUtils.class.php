<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class LogsConfigurationPropertyUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getSaveFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                LogsConfigurationPropertyConst::SAVE_KEYS_WITHOUT_SECTION
            )
        );
    }
    
    public static function
        getSaveFromMapInSection(array $aMap) : string
    {
        foreach(GenericConfigurationPropertyConst::SAVE_KEYS as $key)
        {
            if(GenericConfigurationUtils::
               isBooleanExtendedAccordingToKeyOfMap($aMap, $key))
            {
                return $aMap[$key];
            }
        }
        return self::getSaveFromMapWithoutSection($aMap);
    }
    
    public static function
        getFilePathFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::getPotentielValueFromKeysOfMap(
                $aMap,
                LogsConfigurationPropertyConst::FILE_PATH_KEYS_WITHOUT_SECTION
            )
        );
    }
    
    public static function
        getFilePathFromMapInSection(array $aMap) : string
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'file-path'))
        {
            return $aMap['file-path'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'filepath'))
        {
            return $aMap['filepath'];
        }
        return self::getFilePathFromMapWithoutSection($aMap);
    }
    
    public static function
        getFileNameFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::getPotentielValueFromKeysOfMap(
                $aMap,
                LogsConfigurationPropertyConst::FILE_NAME_KEYS_WITHOUT_SECTION
            )
        );
    }
    
    public static function
        getFileNameFromMapInSection(array $aMap) : string
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'file-name'))
        {
            return $aMap['file-name'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'filename'))
        {
            return $aMap['filename'];
        }
        return self::getFileNameFromMapWithoutSection($aMap);
    }
    
    public static function
        getDirectoryFromMapWithoutSection(array $aMap) : string
    {
        if(GenericConfigurationUtils::
           isOkAccordingToKeysOfMap($aMap, array('logs-use-tmp',
                                                 'log-use-tmp',
                                                 'logs-use-tmp-dir',
                                                 'logs-use-temp-dir',
                                                 'use-tmp-for-logs',
                                                 'use-temp-for-logs',
                                                 'use-temp-dir-for-logs',
                                                 'use-tmp-for-log')))
        {
            return FileSystemUtils::getSystemTemporaryDirectory();
        }
        return (
            GenericConfigurationUtils::getPotentielValueFromKeysOfMap(
                $aMap,
                LogsConfigurationPropertyConst::DIRECTORY_KEYS_WITHOUT_SECTION
            )
        );
    }
    
    public static function
        getDirectoryFromMapInSection(array $aMap) : string
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'directory'))
        {
            return $aMap['directory'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'dir'))
        {
            return $aMap['dir'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'folder'))
        {
            return $aMap['folder'];
        }
        if(GenericConfigurationUtils::
           isOkAccordingToKeysOfMap($aMap,
                                    GenericConfigurationPropertyConst::
                                    USE_SYSTEM_TEMPORARY_DIRECTORY_KEYS))
        {
            return FileSystemUtils::getSystemTemporaryDirectory();
        }
        return self::getDirectoryFromMapWithoutSection($aMap);
    }
    
    public static function
        getFormatAsStringFromMapWithoutSection(array $aMap) : string
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'logs-format'))
        {
            return $aMap['logs-format'];
        }
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'log-format'))
        {
            return $aMap['log-format'];
        }
        return '';
    }
    
    public static function
        getFormatAsStringFromMapInSection(array $aMap) : string
    {
        if(ArrayUtils::keyExistsWithNotEmptyValue($aMap, 'format'))
        {
            return $aMap['format'];
        }
        return self::getFormatAsStringFromMapWithoutSection($aMap);
    }
    
    public static function
        getShowFromMapWithoutSection(array $aMap) : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                LogsConfigurationPropertyConst::SHOW_KEYS_WITHOUT_SECTION
            )
        );
    }
    
    public static function
        getShowFromMapInSection(array $aMap) : string
    {
        foreach(GenericConfigurationPropertyConst::SHOW_KEYS as $key)
        {
            if(GenericConfigurationUtils::
               isBooleanExtendedAccordingToKeyOfMap($aMap, $key))
            {
                return $aMap[$key];
            }
        }
        return self::getShowFromMapWithoutSection($aMap);
    }
}
