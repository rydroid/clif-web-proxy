<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UserInterfaceConfigurationPropertyUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getShowDebugFromMapWithoutSection(array $aMap)
        : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                UserInterfaceConfigurationPropertyConst::
                SHOW_DEBUG_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getShowDebugFromMapInSection(array $aMap)
        : string
    {
        return self::getShowDebugFromMapWithoutSection($aMap);
    }
    
    public static function
        getShowProxyGetParametersConfigurationFromMapWithoutSection(array $aMap)
        : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                UserInterfaceConfigurationPropertyConst::
                SHOW_PROXY_GET_PARAMETERS_CONFIGURATION_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getShowProxyGetParametersConfigurationFromMapInSection(array $aMap)
        : string
    {
        return (
            self::
            getShowProxyGetParametersConfigurationFromMapWithoutSection($aMap)
        );
    }
    
    public static function
        getShowProxyCookiesConfigurationFromMapWithoutSection(array $aMap)
        : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                UserInterfaceConfigurationPropertyConst::
                SHOW_PROXY_COOKIES_CONFIGURATION_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getShowProxyCookiesConfigurationFromMapInSection(array $aMap)
        : string
    {
        return (
            self::
            getShowProxyCookiesConfigurationFromMapWithoutSection($aMap)
        );
    }
    
    public static function
        getUseClientScriptsFromMapWithoutSection(array $aMap)
        : string
    {
        return (
            GenericConfigurationUtils::
            getPotentielBooleanExtendedValueFromKeysOfMap(
                $aMap,
                UserInterfaceConfigurationPropertyConst::
                USE_CLIENT_SCRIPTS_WITHOUT_SECTION_KEYS
            )
        );
    }
    
    public static function
        getUseClientScriptsFromMapInSection(array $aMap)
        : string
    {
        return self::getUseClientScriptsFromMapWithoutSection($aMap);
    }
}
