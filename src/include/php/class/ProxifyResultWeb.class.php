<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class ProxifyResultWeb
    extends ProxifyResultWithErrors
{
    private $content;
    private $header;
    
    
    public function
        __construct(ProxifyResultMetaData $metadata,
                    string $content,
                    string $header = '',
                    array $errors = [])
    {
        parent::__construct($metadata, $errors);
        $this->header = $header;
        $this->setContent($content);
    }
    
    
    public function
        getContent() : string
    {
        return $this->content;
    }
    
    public function
        getHeaderAsString() : string
    {
        return $this->header;
    }
    
    public function
        getHeader() : HttpHeaders
    {
        return HttpHeaders::createFromString($this->header);
    }
    
    
    public function
        proxifyString(string $content) : string
    {
        $url = $this->getUrl();
        if(UrlTextStringUtils::isHtml($url))
        {
            return
                GeneralProxifierUtils::proxifyHtmlStringToString($content);
        }
        if(UrlTextStringUtils::isCss($url) ||
           HttpHeadersContentTypeStringUtils::isCss($this->header))
        {
            return
                GeneralProxifierUtils::proxifyCssStringToString($content);
        }
        return GeneralProxifierUtils::proxifyContent($content);
    }
    
    public function
        setContent(string $content)
    {
        $this->content = $this->proxifyString($content);
    }
}
