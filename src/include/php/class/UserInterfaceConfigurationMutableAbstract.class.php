<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


abstract class UserInterfaceConfigurationMutableAbstract
    extends UserInterfaceConfigurationAbstract
{
    public abstract function
        setShowDebug(OptionnalBoolean $value);
    
    public function
        setShowDebugWithString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setShowDebug(OptionnalBooleanUtils::createFromString($value));
        return true;
    }
    
    
    public abstract function
        setShowProxyGetParametersConfiguration(OptionnalBoolean $value);
    
    public function
        setShowProxyGetParametersConfigurationWithString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setShowProxyGetParametersConfiguration(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setShowProxyCookiesConfiguration(OptionnalBoolean $value);
    
    public function
        setShowProxyCookiesConfigurationWithString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setShowProxyCookiesConfiguration(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
    
    
    public abstract function
        setUseClientScripts(OptionnalBoolean $value);
    
    public function
        setUseClientScriptsWithString(string $value) : bool
    {
        if(!OptionnalBooleanUtils::isValidString($value))
        {
            return false;
        }
        $this->setUseClientScripts(
            OptionnalBooleanUtils::createFromString($value)
        );
        return true;
    }
}
