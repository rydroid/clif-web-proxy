<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierStyleAttribute
    extends DomDocumentProxifierDecorator
{
    private $proxifier;
    
    
    public function
        __construct(DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
        $this->proxifier = new CssStringProxifier();
    }
    
    public function
        setUrl(string $url) : bool
    {
        return parent::setUrl($url) && $this->proxifier->setUrl($url);
    }
    
    
    public function
        getXPathQuery() : string
    {
        return '/html/body[string(@style)] | /html/body//*[string(@style)]';
    }
    
    public function
        getElements(DOMDocument $document) : DOMNodeList
    {
        $pageXPath = new DOMXPath($document);
        $queryString = $this->getXPathQuery();
        return $pageXPath->query($queryString);
    }
    
    public function
        proxifyElement(DOMElement& $element) : bool
    {
        if(!$element->hasAttribute('style'))
        {
            return false;
        }
        
        $attrValueOrigin = $element->getAttribute('style');
        if(empty(trim($attrValueOrigin)))
        {
            return false;
        }
        
        $attrValueProxified = $this->proxifier->proxifyString(
            $attrValueOrigin
        );
        if($attrValueOrigin == $attrValueProxified)
        {
            return false;
        }
        $attrValueOrigin = null;
        $element->setAttribute('style', $attrValueProxified);
        return true;
    }
    
    public function
        proxifyNodeList(DOMNodeList& $elements) : int
    {
        $nbModified = 0;
        foreach($elements as $element)
        {
            if($this->proxifyElement($element))
            {
                ++$nbModified;
            }
        }
        return $nbModified;
    }
    
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        $elements = $this->getElements($document);
        $nbModified += $this->proxifyNodeList($elements);
        return $nbModified;
    }
}
