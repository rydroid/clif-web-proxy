<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @SuppressWarnings(PHPMD.Superglobals)
 */
final class ServerUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        getProtocol() : string
    {
        return $_SERVER['SERVER_PROTOCOL'];
    }
    
    public static function
        getAddress() : string
    {
        return $_SERVER['SERVER_ADDR'];
    }
    
    public static function
        getName() : string
    {
        if(!isset($_SERVER['SERVER_NAME']))
        {
            return '';
        }
        return $_SERVER['SERVER_NAME'];
    }
    
    public static function
        useHttps() : bool
    {
        return
            isset($_SERVER['HTTPS']) &&
            $_SERVER['HTTPS'] != null &&
            $_SERVER['HTTPS'] != '';
    }
    
    public static function
        getScriptName() : string
    {
        return $_SERVER['SCRIPT_NAME'];
    }
    
    public static function
        getUrl() : string
    {
        $url = UrlStringUtils::concatenate(
            self::getName(), self::getScriptName()
        );
        if(self::useHttps())
        {
            return 'https://'. $url;
        }
        return 'http://'. $url;
    }
    
    public static function
        getUrlWithoutFile() : string
    {
        return UrlStringParseUtils::getWithoutFile(self::getUrl());
    }
    
    public static function
        getRequestMethod() : string
    {
        if(!isset($_SERVER['REQUEST_METHOD']))
        {
            return '';
        }
        return trim($_SERVER['REQUEST_METHOD']);
    }
    
    public static function
        getUserAndPasswordString() : string
    {
        $result = $_SERVER['PHP_AUTH_USER'];
        if(isset($_SERVER['PHP_AUTH_PW']))
        {
            $result .=  ':'. $_SERVER['PHP_AUTH_PW'];
        }
        return $result;
    }
    
    
    /**
     * @see https://github.com/ralouphie/getallheaders
     * 
     * @author Ralph Khattar (2014-2016)
     * 
     * @copyright
     * Permission is hereby granted, free of charge, to any person obtaining
     * a copy of this software and associated documentation files
     * (the "Software"), to deal in the Software without restriction,
     * including without limitation the rights to
     * use, copy, modify, merge, publish, distribute, sublicense,
     * and/or sell copies of the Software, and to permit persons to whom
     * the Software is furnished to do so, subject to the following conditions:
     * 
     * @copyright
     * The above copyright notice and this permission notice shall be included
     * in all copies or substantial portions of the Software.
     * 
     * @copyright
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
     * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
     * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
     * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
     * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
     * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
     * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     * 
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public static function
        getHeadersPolyfill() : array
    {
        $copyServer = array(
            'CONTENT_TYPE'   => 'Content-Type',
            'CONTENT_LENGTH' => 'Content-Length',
            'CONTENT_MD5'    => 'Content-Md5',
        );
        
        $headers = array();
        
        foreach($_SERVER as $key => $value)
        {
            if(strtoupper(substr($key, 0, 5)) === 'HTTP_')
            {
                $key = substr($key, 5);
                if(!isset($copyServer[$key]) || !isset($_SERVER[$key]))
                {
                    $subject = ucwords(strtolower(str_replace('_', ' ', $key)));
                    $key = str_replace(' ', '-', $subject);
                    $headers[$key] = $value;
                }
            }
            else if(isset($copyServer[$key]))
            {
                $headers[$copyServer[$key]] = $value;
            }
        }
        
        if(!isset($headers['Authorization']))
        {
            if(isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']))
            {
                $headers['Authorization'] = (
                    $_SERVER['REDIRECT_HTTP_AUTHORIZATION']
                );
            }
            else if(isset($_SERVER['PHP_AUTH_USER']))
            {
                $userPass = self::getUserAndPasswordString();
                $headers['Authorization'] = 'Basic ' . base64_encode($userPass);
            }
            else if(isset($_SERVER['PHP_AUTH_DIGEST']))
            {
                $headers['Authorization'] = $_SERVER['PHP_AUTH_DIGEST'];
            }
        }
        
        return $headers;
    }
    
    public static function
        getHeaders() : array
    {
        if(function_exists('getallheaders'))
        {
            return getallheaders();
        }
        return self::getHeadersPolyfill();
    }
}
