<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class UrlTextStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        isXml(string $url) : bool
    {
        return StringUtils::endsWithInsensitiveArray(
            UrlStringParseUtils::getFileName($url),
            ['.xml', '.xhtml', '.svg']
        );
    }
    
    public static function
        isJson(string $url) : bool
    {
        return StringUtils::endsWithInsensitive(
            UrlStringParseUtils::getFileName($url), '.json'
        );
    }
    
    public static function
        isHtml(string $url) : bool
    {
        return StringUtils::endsWithInsensitiveArray(
            UrlStringParseUtils::getFileName($url),
            ['.html', '.xhtml', '.htm']
        );
    }
    
    public static function
        isCss(string $url) : bool
    {
        return StringUtils::endsWithInsensitive(
            UrlStringParseUtils::getFileName($url), '.css'
        );
    }
}
