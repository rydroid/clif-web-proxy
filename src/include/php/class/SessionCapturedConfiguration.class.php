<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class SessionCapturedConfiguration
    extends SessionCapturedConfigurationMutableAbstract
{
    private $directory;
    private $fileName;
    private $save;
    private $saveProxy;
    private $httpConfiguration;
    
    
    public function
        __construct()
    {
        $this->directory         = '';
        $this->fileName          = '';
        $this->save              = OptionnalBooleanUtils::createUndefined();
        $this->createAuto        = OptionnalBooleanUtils::createUndefined();
        $this->saveProxy         = OptionnalBooleanUtils::createUndefined();
        $this->httpConfiguration = new HttpSessionCapturedConfiguration();
    }
    
    
    public function
        getDirectory() : string
    {
        return $this->directory;
    }
    
    public function
        setDirectory(string $value)
    {
        $this->directory = $value;
    }
    
    public function
        getFileName() : string
    {
        return $this->fileName;
    }
    
    public function
        setFileName(string $value)
    {
        $this->fileName = $value;
    }
    
    
    public function
        hasToSave() : OptionnalBoolean
    {
        return $this->save;
    }
    
    public function
        setSave(OptionnalBoolean $value)
    {
        $this->save = $value;
    }
    
    
    public function
        hasToCreateAutomatically() : OptionnalBoolean
    {
        return $this->createAuto;
    }
    
    public function
        setCreateAutomatically(OptionnalBoolean $value)
    {
        $this->createAuto = $value;
    }
    
    
    public function
        hasToSaveProxy() : OptionnalBoolean
    {
        return $this->saveProxy;
    }
    
    public function
        setSaveProxy(OptionnalBoolean $value)
    {
        $this->saveProxy = $value;
    }
    
    
    public function
        getHttp()
        : HttpSessionCapturedConfigurationAbstract
    {
        return $this->httpConfiguration;
    }
    
    public function
        getMutableHttp()
        : HttpSessionCapturedConfigurationMutableAbstract
    {
        return $this->httpConfiguration;
    }
}
