<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class HtmlDocumentProxifierMetaRefresh
    extends DomDocumentProxifierDecorator
{
    public function
        __construct(DomDocumentProxifierAbstract $previousProxy = null)
    {
        parent::__construct($previousProxy);
    }
    
    
    public function
        getXPathQuery() : string
    {
        return (
            '/html/head//meta['.
            XPathUtils::lowerCase('@http-equiv').'=\'refresh\' and '.
            'contains('. XPathUtils::lowerCase('@content'). ', \'url=\')]'
        );
    }
    
    public function
        getElements(DOMDocument $document) : DOMNodeList
    {
        $docXPath = new DOMXPath($document);
        $queryString = $this->getXPathQuery();
        $elements = $docXPath->query($queryString);
        return $elements;
    }
    
    public function
        isElementWithNeededAttributes(DOMElement $element) : bool
    {
        return (
            $element->hasAttribute('http-equiv') &&
            $element->hasAttribute('content')
        );
    }
    
    public function
        isElementWithNeededNotEmptyAttributes(DOMElement $element) : bool
    {
        if(!$this->isElementWithNeededAttributes($element))
        {
            return false;
        }
        return (
            !empty(trim($element->getAttribute('http-equiv'))) &&
            !empty(trim($element->getAttribute('content')))
        );
    }
    
    private function
        proxifyElement(DOMElement& $element) : bool
    {
        if(!$this->isElementWithNeededNotEmptyAttributes($element))
        {
            return false;
        }
        
        if(strtolower($element->getAttribute('http-equiv')) != 'refresh')
        {
            return false;
        }
        $attributeValue = $element->getAttribute('content');
        if(empty(trim($attributeValue)))
        {
            return false;
        }
        
        $position = stripos($attributeValue, 'url=');
        if($position === FALSE)
        {
            return false;
        }
        $position += 4;
        
        $url = substr($attributeValue, $position);
        if(empty(trim($url)))
        {
            return false;
        }
        
        $urlProxified = $this->proxifyUrl($url);
        $attributeBegin = substr($attributeValue, 0, $position);
        $newAttributeValue = $attributeBegin . $urlProxified;
        $element->setAttribute('content', $newAttributeValue);
        return true;
    }
    
    public function
        proxifyDocument(DOMDocument $document) : int
    {
        $nbModified = parent::proxifyDocument($document);
        
        $elements = $this->getElements($document);
        foreach($elements as $element)
        {
            if($this->proxifyElement($element))
            {
                ++$nbModified;
            }
        }
        
        return $nbModified;
    }
}
