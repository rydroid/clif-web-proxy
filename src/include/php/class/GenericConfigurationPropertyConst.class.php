<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class GenericConfigurationPropertyConst
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    const SAVE_KEYS = [
        'save', 'has-to-save', 'hastosave', 'must-save'
    ];
    
    const SHOW_KEYS = [
        'show', 'can-be-shown', 'canbeshown'
    ];
    
    const DIRECTORY_KEYS = [
        'directory', 'dir', 'folder', 'dossier'
    ];
    
    const USE_SYSTEM_TEMPORARY_DIRECTORY_KEYS = [
        'use-tmp',
        'use-tmp-dir',       'use-temp-dir',
        'use-tmp-directory', 'use-temp-directory',
        'use-temporary-dir', 'use-temporary-directory',
        'use-tmp-folder',    'use-temp-folder'
    ];
    
    const CREATE_AUTO_KEYS = [
        'create-auto', 'create-automatically',
        'autocreate', 'auto-create',
        'créer-automatiquement'
    ];
}
