<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class DebugUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $debug = false;
    
    
    public static function
        isOn() : bool
    {
        return self::$debug;
    }
    
    public static function
        setOn()
    {
        self::$debug = true;
    }
    
    public static function
        setOff()
    {
        self::$debug = false;
    }
}
