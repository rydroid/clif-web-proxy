<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxyConfigurationPrinterUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        toStringAsHtmlList(ProxyConfigurationAbstract $configuration,
                        string $linePrefix = '',
                        string $lineEnd = PHP_EOL)
        : string
    {
        if($configuration->isNull())
        {
            return ' none'. $lineEnd;
        }
        
        $result = '';
        $result .= PHP_EOL . $linePrefix .'<ul>'. $lineEnd;
        $result .= $linePrefix .'<li>URL: ';
        $result .= $configuration->getUrl() .'</li>'. $lineEnd;
        if($configuration->hasPort())
        {
            $result .= $linePrefix .'<li>Port number: ';
            $result .= $configuration->getPort();
            $result .= '</li>' . $lineEnd;
        }
        $result .= '</ul>'. $lineEnd;
        return $result;
    }
    
    public static function
        printAsHtmlList(ProxyConfigurationAbstract $configuration,
                        string $linePrefix = '',
                        string $lineEnd = PHP_EOL)
    {
        echo self::toStringAsHtmlList($configuration, $linePrefix, $lineEnd);
    }
}
