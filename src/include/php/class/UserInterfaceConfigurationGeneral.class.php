<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class UserInterfaceConfigurationGeneral
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    private static $conf = null;
    
    
    public static function
        loadForce()
    {
        self::$conf = new UserInterfaceConfigurationOrderedList(
            array(
                new UserInterfaceConfigurationDefaultValues()
            )
        );
    }
    
    public static function
        load() : bool
    {
        if(self::$conf == null)
        {
            self::loadForce();
        }
        return self::$conf != null && count(self::$conf) > 0;
    }
    
    public static function
        get() : UserInterfaceConfigurationAbstract
    {
        if(self::load())
        {
            return self::$conf;
        }
        return new UserInterfaceConfigurationNull();
    }
    
    
    public static function
        showDebug() : bool
    {
        return self::get()->showDebug()->isTrue();
    }
    
    public static function
        showProxyGetParametersConfiguration() : bool
    {
        return self::get()->showProxyGetParametersConfiguration()->isTrue();
    }
    
    public static function
        showProxyCookiesConfiguration() : bool
    {
        return self::get()->showProxyCookiesConfiguration()->isTrue();
    }
    
    public static function
        useClientScripts() : bool
    {
        return self::get()->useClientScripts()->isTrue();
    }
}
