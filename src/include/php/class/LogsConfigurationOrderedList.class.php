<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class LogsConfigurationOrderedList
    extends LogsConfigurationAbstract
    implements Countable
{
    private $configurations;
    
    
    public function
        __construct(array $configurations)
    {
        $this->configurations = array();
        $this->addMany($configurations);
    }
    
    
    public function
        clear()
    {
        $this->configurations = array();
    }
    
    public function
        addOne(LogsConfigurationAbstract $configuration) : bool
    {
        if(in_array($configuration, $this->configurations))
        {
            return false;
        }
        $this->configurations[] = $configuration;
        return true;
    }
    
    public function
        addMany(array $configurations) : int
    {
        $nbAdded = 0;
        foreach($configurations as $configuration)
        {
            if($configuration instanceof LogsConfigurationAbstract)
            {
                if($this->addOne($configuration))
                {
                    ++$nbAdded;
                }
            }
        }
        return $nbAdded;
    }
    
    public function
        count() : int
    {
        return count($this->configurations);
    }
    
    
    public function
        hasToSave() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->hasToSave();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
    
    public function
        getFilePath() : string
    {
        foreach($this->configurations as $configuration)
        {
            if($configuration->hasFilePath())
            {
                return $configuration->getFilePath();
            }
        }
        return '';
    }
    
    public function
        canBeShown() : OptionnalBoolean
    {
        foreach($this->configurations as $configuration)
        {
            $value = $configuration->canBeShown();
            if($value->isDefined())
            {
                return $value;
            }
        }
        return OptionnalBooleanUtils::createUndefined();
    }
}
