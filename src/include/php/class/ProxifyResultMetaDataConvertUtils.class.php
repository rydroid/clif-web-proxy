<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


final class ProxifyResultMetaDataConvertUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        toDomElement(ProxifyResultMetaData $metadata,
                     DOMDocument $document,
                     SessionCapturedConfigurationAbstract $configuration)
        : DOMElement
    {
        $element = $document->createElement('resource');
        
        if($metadata->hasUrl())
        {
            $urlElement = $document->createElement('url');
            $urlElement->setAttribute('value', $metadata->getUrl());
            $element->appendChild($urlElement);
        }
        
        $method = ServerUtils::getRequestMethod();
        if(!empty($method))
        {
            $methodElement = $document->createElement('method');
            $methodElement->setAttribute('value', $method);
            $element->appendChild($methodElement);
        }
        
        if($metadata->isValidTimeRequest())
        {
            $timeRequestElement = TimeXmlUtils::toDomElement(
                $document,
                $metadata->getTimeRequestMicroSecond(),
                'timeRequest', 'us', 'unix-timestamp'
            );
            $element->appendChild($timeRequestElement);
        }
        
        if($metadata->isValidTimeResponse())
        {
            $timeResponseElement = TimeXmlUtils::toDomElement(
                $document,
                $metadata->getTimeResponseMicroSecond(),
                'timeResponse', 'us', 'unix-timestamp'
            );
            $element->appendChild($timeResponseElement);
        }
        
        if($metadata->hasHeadersClient())
        {
            $headers = (
                ProxifyResultMetaDataUtils::
                getHeadersClientCloneRespectingConfiguration(
                    $metadata, $configuration->getHttp()
                )
            );
            $headersElement = HttpHeadersUtils::toDomElement(
                $headers, $document, 'client'
            );
            $element->appendChild($headersElement);
        }
        
        if(!PostParametersUtils::isEmpty())
        {
            $postElement = PostParametersUtils::toDomElement($document);
            $element->appendChild($postElement);
        }
        
        if($configuration->hasToSaveProxy() &&
           !ProxyConfigurationGeneral::isNull())
        {
            $proxyElement = ProxyConfigurationUtils::toDomElement(
                ProxyConfigurationGeneral::get(), $document
            );
            $element->appendChild($proxyElement);
        }
        
        return $element;
    }
    
    public static function
        toDomElementDefault(ProxifyResultMetaData $metadata,
                            DOMDocument $document)
        : DOMElement
    {
        return self::toDomElement(
            $metadata, $document, SessionCapturedConfigurationGeneral::get()
        );
    }
}
