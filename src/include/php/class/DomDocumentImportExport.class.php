<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


class DomDocumentImportExport
    extends DocumentImportExport
{
    private $document;
    
    
    /**
     * @SuppressWarnings(ElseExpression)
     */
    public function
        __construct(DOMDocument $document = null)
    {
        if($document == null)
        {
            $this->document = new DOMDocument();
        }
        else
        {
            $this->document = $document;
        }
    }
    
    
    public function
        getDomDocument() : DOMDocument
    {
        return $this->document;
    }
    
    public function
        loadFromText(string $text) : bool
    {
        return $this->document->loadXML($text);
    }
    
    public function
        saveToText() : string
    {
        return $this->document->saveXML();
    }
}
