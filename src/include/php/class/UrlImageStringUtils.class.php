<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


final class UrlImageStringUtils
{
    /**
     * Don't let anyone instantiate this class.
     */
    private function
        __construct()
    {}
    
    
    public static function
        isJpeg(string $url) : bool
    {
        return StringUtils::endsWithInsensitiveArray(
            UrlStringParseUtils::getFileName($url),
            ['.jpg', '.jpeg']
        );
    }
    
    public static function
        isPng(string $url) : bool
    {
        return StringUtils::endsWithInsensitive($url, '.png');
    }
    
    public static function
        isGif(string $url) : bool
    {
        return StringUtils::endsWithInsensitive($url, '.gif');
    }
    
    public static function
        isWebp(string $url) : bool
    {
        return StringUtils::endsWithInsensitive($url, '.webp');
    }
    
    public static function
        isSvg(string $url) : bool
    {
        return StringUtils::endsWithInsensitive($url, '.svg');
    }
    
    
    public static function
        isBitmapImage(string $url) : bool
    {
        return (
            self::isJpeg($url) ||
            self::isPng($url)  ||
            self::isGif($url)  ||
            self::isWebp($url)
        );
    }
    
    public static function
        isVectorImage(string $url) : bool
    {
        return self::isSvg($url);
    }
    
    public static function
        isImage(string $url) : bool
    {
        return self::isBitmapImage($url) || self::isVectorImage($url);
    }
}
