/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


function getXsltProcessor()
{
    if(typeof(XSLTProcessor) != 'function' ||
       typeof(XSLTProcessor.prototype.importStylesheet) != 'function' ||
       typeof(XSLTProcessor.prototype.transformToDocument) != 'function')
    {
	return null;
    }
    return new XSLTProcessor();
}
