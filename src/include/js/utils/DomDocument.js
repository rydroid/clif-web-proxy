/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


/**
 * @param {Object} oldDocument
 * @return {Object}
 */
function domDocumentClone(oldDocument)
{
    "use strict";
    var newDocument = oldDocument.implementation.createDocument(
        oldDocument.namespaceURI, // namespace to use
        null, // name of the root element (or for empty document)
        null  // doctype (null for XML)
    );
    var newNode = newDocument.importNode(
        oldDocument.documentElement, // node to import
        true                         // clone its descendants
    );
    newDocument.appendChild(newNode);
    return newDocument;
}
