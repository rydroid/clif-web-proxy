/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @param  {Object}   anElement
 * @param  {Function} aFunction
 * @param  {boolean}  useCapture
 * @return {boolean}
 */
function addOnClickEventToElement(anElement, aFunction, useCapture)
{
    'use strict';
    if(typeof(anElement) != 'object')
    {
	return false;
    }
    if(typeof(useCapture) == 'boolean')
    {
	useCapture = false;
    }
    if(typeof(window.addEventListener) == 'function')
    {
	anElement.addEventListener('click', aFunction, useCapture);
    }
    else
    {
	anElement.onclick = aFunction;
    }
    return true;
}

/**
 * @return {boolean}
 */
function confirmDestroySession()
{
    'use strict'
    return confirm(
        'Destroy the session? '+
        'It can not be undone, '+
        'all the data related will be definitively removed.'
    );
}

/**
 * @return {boolean}
 */
function confirmReinitializeSession()
{
    'use strict'
    return confirm(
        'Create a new session? '+
        'The current session (if exists) will be destroyed. '+
        'It can not be undone, '+
        'all the data related will be definitively removed.'
    );
}

/**
 * @return {Array|NodeList}
 */
function getLinksToDestroySession()
{
    'use strict'
    if(typeof(document.querySelectorAll) != 'function')
    {
        return [];
    }
    return document.querySelectorAll('a[href^="session-destroy"');
}

/**
 * @return {Array|NodeList}
 */
function getLinksToReinitializeSession()
{
    'use strict'
    if(typeof(document.querySelectorAll) != 'function')
    {
        return [];
    }
    return document.querySelectorAll('a[href^="session-reinitialize"');
}

function addEventsToLinksToDestroySession()
{
    'use strict';
    var elements = getLinksToDestroySession();
    for(var index=0; index < elements.length; ++index)
    {
        var element = elements[index];
        element.onclick = confirmDestroySession;
    }
}

function addEventsToLinksToReinitializeSession()
{
    'use strict';
    var elements = getLinksToReinitializeSession();
    for(var index=0; index < elements.length; ++index)
    {
        var element = elements[index];
        element.onclick = confirmReinitializeSession;
    }
}

function onHomePageLoaded()
{
    'use strict';
    addEventsToLinksToDestroySession();
    addEventsToLinksToReinitializeSession();
}

if(typeof(window.addEventListener) == 'function')
{
    window.addEventListener('load', onHomePageLoaded, false);
}
else
{
    window.onload = onHomePageLoaded;
}
