/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


(function()
 {
    /**
     * Returns true if a variable is a string and false otherwise
     * @param {*} variable A variable to evaluate
     * @return {boolean} True if the variable is a string and false otherwise
     */
    function isString(variable)
    {
	"use strict";
	return typeof(variable) == 'string' || variable instanceof String;
    }
    
    /**
     * Returns true if a variable is a string and empty, otherwise false
     * @param {*} variable A variable to evaluate
     * @return {boolean} If the variable is a string and empty
     */
    function isEmptyString(variable)
    {
	"use strict";
	return isString(variable) && variable.length == 0;
    }
    
    /**
     * Returns true if a variable is a string and not empty, otherwise false
     * @param {*} variable A variable to evaluate
     * @return {boolean} If the variable is a string and not empty
     */
    function isNotEmptyString(variable)
    {
	"use strict";
	return isString(variable) && variable.length > 0;
    }
    
    /**
     * Removes whitespace from both ends of the string
     * @param {String|string} aString A string to trim
     * @return {String|string} string trimmed
     */
    function trimString(aString)
    {
	if(typeof(String.prototype.trim) == 'function')
	{
	    return aString.trim();
	}
	return aString.replace(/^\s+|\s+$/g, '');
    };
    
    /**
     * Returns if a variable is a string and empty after trim
     * @param {*} variable A variable to evaluate
     * @return {boolean} If the variable is a string and empty after trim
     */
    function isEmptyStringAfterTrim(variable)
    {
	"use strict";
	return (
	    isString(variable) && trimString(new String(variable)).length == 0
	);
    }
    
    /**
     * Returns if a variable is a string and not empty after trim
     * @param {*} variable A variable to evaluate
     * @return {boolean} If the variable is a string and not empty after trim
     */
    function isNotEmptyStringAfterTrim(variable)
    {
	"use strict";
	return (
	    isString(variable) && trimString(new String(variable)).length > 0
	);
    }
    
    
    function isScriptElement(variable)
    {
	"use strict";
	return (
	    isNotEmptyStringAfterTrim(variable.localName) &&
	    variable.localName.toLowerCase() == 'script'
	);
    }
    
    /**
     * @suppress {undefinedVars}
     */
    function getHtmlScriptElementClass()
    {
	if(typeof(HTMLScriptElementPrototype) == 'function' &&
	   typeof(HTMLScriptElementPrototype.prototype.setAttribute)
	   == 'function')
	{
	    return HTMLScriptElementPrototype;
	}
	if(typeof(HTMLScriptElement) == 'function' &&
	   typeof(HTMLScriptElement.prototype.setAttribute) == 'function')
	{
	    return HTMLScriptElement;
	}
	if(typeof(HTMLElementPrototype) == 'function' &&
	   typeof(HTMLElementPrototype.prototype.setAttribute) == 'function')
	{
	    return HTMLElementPrototype;
	}
	if(typeof(HTMLElement) == 'function' &&
	   typeof(HTMLElement.prototype.setAttribute) == 'function')
	{
	    HTMLElement.prototype.setAttribute = scriptSrcSetAndProxify;
	}
	if(typeof(ElementPrototype) == 'function' &&
	   typeof(ElementPrototype.prototype.setAttribute) == 'function')
	{
	    return ElementPrototype;
	}
	if(typeof(Element) == 'function' &&
	   typeof(Element.prototype.setAttribute) == 'function')
	{
	    return Element;
	}
	return null;
    }
    
    
    function classExtends(classToExtend, parentClass)
    {
	"use strict";
	classToExtend.prototype = Object.create(parentClass.prototype);
	classToExtend.prototype.constructor = classToExtend;
    }
    
    /**
     * @param {Object} anObject
     */
    function getNamesOfMethodsOfObject(anObject)
    {
	"use strict";
	
	if(typeof(Object) != 'function' ||
	   typeof(Object.getOwnPropertyNames) != 'function')
	{
	    return [];
	}
	
	var propertyNames = [];
	if(anObject.prototype)
	{
	    propertyNames = Object.getOwnPropertyNames(anObject.prototype);
	}
	else if(anObject.__proto__)
	{
	    propertyNames = Object.getOwnPropertyNames(anObject.__proto__);
	}
	
	var methodNames = propertyNames.filter(function (name) {
	    return typeof(anObject[name]) === 'function';
	});
	return methodNames;
    }
    
    
    var StringUtils = {};
    
    /**
     * @param {String|string} aString
     * @return {boolean}
     */
    StringUtils.isEmpty = function(aString)
    {
	"use strict";
	return aString.length == 0;
    };
    
    /**
     * @param {String|string} aString
     * @return {boolean}
     */
    StringUtils.isNotEmpty = function(aString)
    {
	"use strict";
	return aString.length > 0;
    };
    
    /**
     * @param {String|string} aString
     * @param {String|string} searchString
     * @param {number=} position
     * @return {boolean}
     */
    StringUtils.startsWithPolyfill = function(aString, searchString, position)
    {
	position = position || 0;
	return aString.substr(position, searchString.length) === searchString;
    };
    
    /**
     * @param {String|string} aString
     * @param {String|string} searchString
     * @param {number=} position
     * @return {boolean}
     */
    StringUtils.startsWith = function(aString, searchString, position)
    {
	if(typeof(position) != 'number')
	{
	    position = 0;
	}
	if(typeof(String.prototype.startsWith) == 'function' &&
	   typeof(aString.startsWith) == 'function')
	{
	    return aString.startsWith(searchString, position);
	}
	return this.startsWithPolyfill(aString, searchString, position);
    };
    
    /**
     * @param {String|string} aString
     * @param {String|string} searchString
     * @param {number=} position
     * @return {boolean}
     */
    StringUtils.startsWithInsensitive = function(
	aString, searchString, position
    )
    {
	return this.startsWith(
	    aString.toLowerCase(), searchString.toLowerCase(), position
	);
    };
    
    /**
     * @param {String|string} aString
     * @param {String|string} searchString
     * @param {number=} position
     * @return {boolean}
     */
    StringUtils.endsWithPolyfill = function(aString, searchString, position)
    {
	if (typeof(position) !== 'number' ||
	    !isFinite(position) ||
	    Math.floor(position) !== position ||
	    position > aString.length)
	{
            position = aString.length;
	}
	position -= searchString.length;
	var lastIndex = aString.lastIndexOf(searchString +"", position);
	return lastIndex !== -1 && lastIndex === position;
    };
    
    /**
     * @param {String|string} aString
     * @param {String|string} searchString
     * @param {number=} length
     * @return {boolean}
     */
    StringUtils.endsWith = function(aString, searchString, length)
    {
	if(typeof(String.prototype.endsWith) == 'function' &&
	   typeof(aString.endsWith) == 'function')
	{
	    return aString.endsWith(searchString, length);
	}
	return this.endsWithPolyfill(aString, searchString, length);
    };
    
    /**
     * @param {String|string} aString
     * @param {String|string} searchString
     * @param {number=} length
     * @return {boolean}
     */
    StringUtils.endsWithInsensitive = function(aString, searchString, length)
    {
	return this.endsWith(
	    aString.toLowerCase(), searchString.toLowerCase(), length
	);
    };
    
    /**
     * @param {String|string} aString
     * @param {string} searchString
     * @param {number=} startPosition
     * @return {boolean}
     */
     StringUtils.contains = function(aString, searchString, startPosition)
     {
	 if(typeof(startPosition) !== 'number')
	 {
	     startPosition = 0;
	 }
	 
	 if(typeof(String.prototype.includes) == 'function' &&
	    typeof(aString.includes) == 'function')
	 {
	     return aString.includes(searchString, startPosition);
	 }
	 if(typeof(String.prototype.contains) == 'function' &&
	    typeof(aString.contains) == 'function')
	 {
	     return aString.contains(searchString, startPosition);
	 }
	 
	 if(startPosition + searchString.length > aString.length)
	 {
	     return false;
	 }
	 return aString.indexOf(searchString, startPosition) !== -1;
     };
    
    
    var UrlProtocolStringUtils = {};
    
    /**
     * @param {String|string} url
     * @return {boolean}
     */
    UrlProtocolStringUtils.isHttp = function(url)
    {
	return StringUtils.startsWithInsensitive(url, 'http://');
    };
    
    /**
     * @param {String|string} url
     * @return {boolean}
     */
    UrlProtocolStringUtils.isHttps = function(url)
    {
	return StringUtils.startsWithInsensitive(url, 'https://');
    };
    
    /**
     * @param {String|string} url
     * @return {boolean}
     */
    UrlProtocolStringUtils.isWeb = function(url)
    {
	return this.isHttp(url) || this.isHttps(url);
    };
    
    
    var UrlStringUtils = {};
    
    /**
     * @param {String|string} params
     * @return {Object}
     */
    UrlStringUtils.parametersStringtoMap = function(params)
    {
	"use strict";
	var searchMap = {};
	var queries = params.replace(/^\?/, '').split('&');
	for(var i = 0; i < queries.length; ++i)
	{
	    var split = queries[i].split('=');
	    if(split.length > 1)
	    {
		searchMap[split[0]] = split[1];
	    }
	}
	return searchMap;
    };
    
    /**
     * @param {String|string} url
     * @return {Object}
     */
    UrlStringUtils.getParametersAsMap = function(url)
    {
	if(typeof(document) != 'object' &&
	   typeof(document.createElement) != 'function')
	{
	    return {};
	}
	
	var parser = document.createElement('a');
	parser.href = url;
	var searchMap = this.parametersStringtoMap(parser.search);
	return searchMap;
    };
    
    /**
     * @param {String|string} url
     * @return {Object}
     */
    UrlStringUtils.toMap = function(url)
    {
	if(typeof(document) != 'object' &&
	   typeof(document.createElement) != 'function')
	{
	    return {};
	}
	
	// Let the browser do the work
	var parser = document.createElement('a');
	parser.href = url;
	
	var protocol = parser.protocol;
	if(isString(protocol) &&
	   protocol.length > 0 &&
	   protocol[protocol.length - 1] == ':')
	{
	    protocol = protocol.substring(0, protocol.length - 1);
	}
	
	// Convert query string to object
	var searchMap = this.parametersStringtoMap(parser.search);
	
	var result = {};
	if(protocol.length > 0)
	{
	    result['protocol'] = protocol;
	}
	if(parser.host.length > 0)
	{
	    result['host']    = parser.host;
	}
	if(parser.hostname.length > 0)
	{
	    result['hostname'] = parser.hostname;
	}
	if(parser.port.length > 0 && !isNaN(parser.port))
	{
	    result['port'] = parser.port;
	}
	if(parser.pathname.length > 0)
	{
	    result['pathname'] = parser.pathname;
	}
	if(parser.search.length > 0)
	{
	    result['searchString'] = parser.search;
	}
	if(searchMap.length > 0)
	{
	    result['searchMap'] = searchMap;
	}
	if(parser.hash.length > 0)
	{
	    result['hash'] = parser.hash;
	}
	return result;
    };
    
    /**
     * @param {Object} components
     * @return {String|string}
     */
    UrlStringUtils.getWithoutDirectoryAndFileNameWithComponents = function(
	components
    )
    {
	"use strict";
	
        var resultStart = null;
        if('protocol' in components &&
           components['protocol'].length > 0)
        {
            resultStart = components['protocol'] + '://';
        }
        
        var login = '';
        if('user' in components &&
           components['user'].length > 0)
        {
            login += components['user'];
        }
        if('pass' in components &&
           components['pass'].length > 0)
        {
            login += ':' + components['pass'];
        }
        if(login.length > 0)
        {
            login += '@';
        }
        
        var resultEnd = '';
        if('hostname' in components &&
           components['hostname'].length > 0)
        {
            resultEnd += components['hostname'];
        }
        if('port' in components &&
           components['port'].length > 0)
        {
            resultEnd += ':' + components['port'];
        }
        
        var result = login + resultEnd + '/';
        if(resultStart != null)
        {
            result = resultStart + result;
        }
        return result;
    };
    
    /**
     * @param {String} url
     * @return {String}
     */
    UrlStringUtils.getWithoutDirectoryAndFileName = function(url)
    {
        "use strict";
        var components = this.toMap(url);
        var result = this.getWithoutDirectoryAndFileNameWithComponents(
            components
        );
        return result;
    };
    
    /**
     * @param {String|string} url
     * @return {String|string}
     */
    UrlStringUtils.getWithoutParametersAndHash = function(url)
    {
	"use strict";
	var components = this.toMap(url);
	var urlResult = this.getWithoutDirectoryAndFileNameWithComponents(
	    components
	);
	if('pathname' in components)
	{
	    urlResult = UrlStringUtils.concatenate(
	        urlResult, components['pathname']
	    );
	}
	return urlResult;
    };
    
    /**
     * @param {String|string} url1
     * @param {String|string} url2
     * @return {String|string}
     */
    UrlStringUtils.concatenate = function(url1, url2)
    {
        "use strict";
        
        if(url1.length == 0)
        {
            return url2;
        }
        if(url2.length == 0)
        {
            return url1;
        }
        
        if(StringUtils.endsWith(url1, '/') &&
           url2[0] == '/')
        {
            return url1 + url2.substr(1);
        }
        if(!StringUtils.endsWith(url1, '/') &&
           url2[0] != '/')
        {
            return url1 +'/'+ url2;
        }
        if(StringUtils.endsWith(url1, '/') &&
           StringUtils.startsWith(url2, './'))
        {
            return url1 + url2.substr(2);
        }
        return url1.concat(url2);
    };
    
    /**
     * @param {String|string} currentUrl
     * @param {String|string} notAbsoluteUrl
     * @return {String|string}
     */
    UrlStringUtils.getAbsolute = function(currentUrl, notAbsoluteUrl)
    {
        "use strict";
        if(notAbsoluteUrl[0] == '/' &&
           (notAbsoluteUrl.length == 1 || notAbsoluteUrl[1] != '/'))
        {
            return this.concatenate(
                this.getWithoutDirectoryAndFileName(currentUrl),
                notAbsoluteUrl
            );
        }
        if(!StringUtils.startsWith(notAbsoluteUrl, '//') &&
           !UrlProtocolStringUtils.isWeb(notAbsoluteUrl))
        {
            return this.concatenate(
                currentUrl, notAbsoluteUrl
            );
        }
        return notAbsoluteUrl;
    };
    
    
    var UrlCurrentUtils = {};
    
    /**
     * @return {String|string}
     */
    UrlCurrentUtils.getAsString = function()
    {
	"use strict";
	if(isNotEmptyStringAfterTrim(window.location.href))
	{
	    return window.location.href;
	}
	if(isNotEmptyStringAfterTrim(document.URL))
	{
	    return document.URL;
	}
	return '';
    };
    
    /**
     * @return {Object}
     */
    UrlCurrentUtils.getParametersAsMap = function()
    {
	"use strict";
        var /** string */ url = UrlCurrentUtils.getAsString();
        var result = UrlStringUtils.getParametersAsMap(url);
        return result;
    };
    
    
    var GetParametersUtils = {};
    
    /**
     * @param {Object} aMap
     * @return {String|string}
     */
    GetParametersUtils.mapToString = function(aMap)
    {
	"use strict";
        var /** string */ result = '';
        for(var key in aMap)
        {
	    var value = aMap[key];
            if(key.length == 0 || value.length == 0)
            {
                continue;
            }
            if(result.length > 0)
            {
                result += '&';
            }
            result += key +'='+ value;
        }
        return result;
    };
    
    
    var ProxifierGetParametersUtils = {};
    
    /**
     * @param {Object} aMap
     * @return {String|string}
     */
    ProxifierGetParametersUtils.getKeyOfUrlToProxifyFromMap = function(aMap)
    {
        "use strict";
        if('url-to-proxify' in aMap &&
	   aMap['url-to-proxify'].length > 0)
        {
            return 'url-to-proxify';
        }
        if('url' in aMap &&
	   aMap['url'].length > 0)
        {
            return 'url';
        }
        if('URL' in aMap &&
	   aMap['URL'].length > 0)
        {
            return 'URL';
        }
        if('q' in aMap &&
	   aMap['q'].length > 0)
        {
            return 'q';
        }
        return '';
    };
    
    /**
     * @param {Object} aMap
     * @return {String|string}
     */
    ProxifierGetParametersUtils.getUrlToProxifyFromMap = function(aMap)
    {
        "use strict";
        var /** string */ key = this.getKeyOfUrlToProxifyFromMap(aMap);
        if(key.length == 0)
        {
            return '';
        }
        return aMap[key];
    };
    
    ProxifierGetParametersUtils.getArrayParamsOfProxifiedUrlFromArray =
	 function(anArray)
    {
        var result = anArray;
        var /** string */ keyToRemove =
	    this.getKeyOfUrlToProxifyFromMap(anArray);
        delete result[keyToRemove];
        return result;
    };
    
    ProxifierGetParametersUtils.getStrOfParamsOfProxifiedUrlFromArray =
	 function(anArray)
    {
        var resultArray = this.getArrayParamsOfProxifiedUrlFromArray(anArray);
        var resultString = GetParametersUtils.mapToString(resultArray);
        return resultString;
    };
    
    /**
     * @return {String|string}
     */
    ProxifierGetParametersUtils.getUrlToProxifyWithParamsFromArray = function(
	anArray
    )
    {
        "use strict";
        var url = this.getUrlToProxifyFromMap(anArray);
        var parametersString = this.getStrOfParamsOfProxifiedUrlFromArray(
            anArray
        );
        if(parametersString.length == 0)
        {
            return url;
        }
        url += '?'+ parametersString;
        return url;
    };
    
    
    var ProxifierGetParametersDefaultUtils = {};
    
    /**
     * @return {String|string}
     */
    ProxifierGetParametersDefaultUtils.getKeyOfUrlToProxify = function()
    {
        "use strict";
        var result = ProxifierGetParametersUtils.getKeyOfUrlToProxifyFromMap(
            UrlStringUtils.getParametersAsMap(UrlCurrentUtils.getAsString())
        );
        return result;
    };
    
    /**
     * @return {String|string}
     */
    ProxifierGetParametersDefaultUtils.getUrlToProxify = function()
    {
        "use strict";
        var result = ProxifierGetParametersUtils.getUrlToProxifyFromMap(
            UrlStringUtils.getParametersAsMap(UrlCurrentUtils.getAsString())
	);
        return result;
    };
    
    /**
     * @return {Object}
     */
    ProxifierGetParametersDefaultUtils.getArrayOfParametersOfProxifiedUrl =
	 function()
    {
        "use strict";
        var res;
        res = ProxifierGetParametersUtils.getArrayParamsOfProxifiedUrlFromArray(
            UrlStringUtils.getParametersAsMap(UrlCurrentUtils.getAsString())
        );
	return res;
    };
    
    /**
     * @return {String|string}
     */
    ProxifierGetParametersDefaultUtils.getStringOfParametersOfProxifiedUrl =
	 function()
    {
        "use strict";
        var res;
        res = ProxifierGetParametersUtils.getStrOfParamsOfProxifiedUrlFromArray(
            UrlStringUtils.getParametersAsMap(UrlCurrentUtils.getAsString())
        );
	return res;
    };
    
    /**
     * @return {String|string}
     */
    ProxifierGetParametersDefaultUtils.getUrlToProxifyWithParameters =
	 function()
    {
        "use strict";
        var res;
        res = ProxifierGetParametersUtils.getUrlToProxifyWithParamsFromArray(
            UrlStringUtils.getParametersAsMap(UrlCurrentUtils.getAsString())
        );
        return res;
    };
    
    
    var PHP = {};
    
    /**
     * @param {String|string} aString
     * @return {String|string}
     * @see https://secure.php.net/manual/en/function.urlencode.php
     * @see http://locutus.io/php/url/urlencode/
     */
    PHP.urlencode = function(aString)
    {
	"use strict";
	var /** string */ result = encodeURIComponent(aString +"");
	result = result.replace(/!/g, '%21');
	result = result.replace(/'/g, '%27');
	result = result.replace(/\(/g, '%28');
	result = result.replace(/\)/g, '%29');
	result = result.replace(/\*/g, '%2A');
	result = result.replace(/%20/g, '+');
	return result;
    };
    
    /**
     * @param {String|string} aString
     * @return {String|string}
     * @see https://secure.php.net/manual/en/function.urldecode.php
     * @see http://locutus.io/php/url/urldecode/
     */
    PHP.urldecode = function(aString)
    {
	"use strict";
	var result = aString;
	result = result.replace(/%(?![\da-f]{2})/gi, function() {
	    // PHP tolerates poorly formed escape sequences
	    return '%25';
	});
	result = result.replace(/\+/g, '%20');
	result = decodeURIComponent(result);
	return result;
    };
    
    
    var UrlProxifierUtils = {};
    
    /**
     * @param {String|string} anUrl
     * @return {String|string}
     */
    UrlProxifierUtils.encodeUrl = function(anUrl)
    {
	"use strict";
	return PHP.urlencode(anUrl);
    };
    
    /**
     * @param {String|string} anUrl
     * @return {String|string}
     */
    UrlProxifierUtils.decodeUrl = function(anUrl)
    {
	"use strict";
	return PHP.urlencode(anUrl);
    };
    
    
    /**
     * @constructor
     * @param {(String|string)=} anUrl
     * @param {(String|string)=} aBaseProxifiedUrl
     */
    function UrlProxifier(anUrl, aBaseProxifiedUrl)
    {
	"use strict";
	
	if(!isString(anUrl))
	{
	    anUrl = UrlCurrentUtils.getAsString();
	}
	/** @private */
	this.url = UrlStringUtils.getWithoutParametersAndHash(
	    new String(anUrl)
	);
	
	if(!isString(aBaseProxifiedUrl))
	{
	    var aMap = UrlCurrentUtils.getParametersAsMap();
	    aBaseProxifiedUrl =
		ProxifierGetParametersUtils.getUrlToProxifyFromMap(aMap);
	    aBaseProxifiedUrl = PHP.urldecode(aBaseProxifiedUrl);
	}
	/** @private */
	this.baseProxifiedUrl = aBaseProxifiedUrl;
    }
    
    /**
     * @return {boolean}
     */
    UrlProxifier.prototype.hasUrl = function()
    {
	"use strict";
	return isNotEmptyString(this.getUrl());
    };
    
    /**
     * @return {String|string}
     */
    UrlProxifier.prototype.getUrl = function()
    {
	"use strict";
	return this.url;
    };
    
    /**
     * @param {String|string} newUrl
     * @return {boolean}
     */
    UrlProxifier.prototype.setUrl = function(newUrl)
    {
	"use strict";
	
	if(!isString(newUrl))
	{
	    return false;
	}
	
	newUrl = trimString(newUrl);
	if(newUrl.length == 0)
	{
	    return false;
	}
	
	this.url = newUrl;
	return true;
    };
    
    /**
     * @param {String|string} anUrl
     * @return {String|string}
     */
    UrlProxifier.prototype.getAbsoluteUrl = function(anUrl)
    {
	"use strict";
	return UrlStringUtils.getAbsolute(
	    new String(this.baseProxifiedUrl), anUrl
	);
    };
    
    /**
     * @param {String|string} anUrl
     * @return {String|string}
     */
    UrlProxifier.prototype.getAbsoluteUrlEncoded = function(anUrl)
    {
	"use strict";
	var /** string */ absoluteUrl = this.getAbsoluteUrl(anUrl);
	return UrlProxifierUtils.encodeUrl(absoluteUrl);
    };
    
    /**
     * @param {String|string} urlToProxify
     * @return {String|string}
     */
    UrlProxifier.prototype.proxifyUrl = function(urlToProxify)
    {
	"use strict";
	
	if(isEmptyStringAfterTrim(urlToProxify) ||
	   StringUtils.startsWith(urlToProxify, this.getUrl()))
	{
	    return urlToProxify;
	}
	
	var /** string */ urlProxified = urlToProxify;
	urlProxified = this.getAbsoluteUrlEncoded(urlProxified);
	urlProxified = this.getUrl() + '?url=' + urlProxified;
	return urlProxified;
    };
    
    
    if(typeof(XMLHttpRequest) == 'function' &&
       typeof(XMLHttpRequest.prototype.open) == 'function')
    {
	/** @const */
	var HTTP_REQUEST_OPEN_NATIVE = XMLHttpRequest.prototype.open;
	var proxifier = new UrlProxifier();
	
	XMLHttpRequest.prototype.open = function()
	{
	    "use strict";
	    try
	    {
		if(arguments != undefined &&
		   arguments != null &&
		   !isNaN(arguments.length) &&
		   arguments.length > 1 &&
		   isNotEmptyStringAfterTrim(arguments[1]))
		{
		    arguments[1] = proxifier.proxifyUrl(arguments[1]);
		}
	    }
	    catch(exception)
	    {
		if(typeof(window.console)       == 'object' &&
		   typeof(window.console.error) == 'function')
		{
		    window.console.error(exception);
		}
	    }
	    HTTP_REQUEST_OPEN_NATIVE.apply(this, arguments);
	};
    }
    
    
    if(typeof(Element) == 'object' ||
       typeof(Element.prototype.setAttribute) == 'function')
    {
	var proxifier = new UrlProxifier();
	
	var scriptElementClass = getHtmlScriptElementClass();
	var /** @const */ ELEMENT_SET_ATTRIBUTE_NATIVE = (
	    scriptElementClass.prototype.setAttribute
	);
	
	scriptElementClass.prototype.setAttribute = function()
	{
	    "use strict";
	    try
	    {
		if(isScriptElement(this) &&
		   arguments != undefined &&
		   arguments != null &&
		   !isNaN(arguments.length) &&
		   arguments.length > 1 &&
		   trimString(arguments[0]).toLowerCase() == 'src' &&
		   isNotEmptyStringAfterTrim(arguments[1]))
		{
		    arguments[1] = proxifier.proxifyUrl(arguments[1]);
		}
	    }
	    catch(exception)
	    {
		if(typeof(window.console)       == 'object' &&
		   typeof(window.console.error) == 'function')
		{
		    window.console.error(exception);
		}
	    }
	    ELEMENT_SET_ATTRIBUTE_NATIVE.apply(this, arguments);
	};
    }
    
    
    /**
     * @constructor
     */
    function TestCase()
    {}
    
    /**
     * @param {boolean} anExpression
     */
    TestCase.prototype.assertTrue = function(anExpression)
    {
	"use strict";
	if(!anExpression)
	{
	    throw new Error('excepted true');
	}
    };
    
    /**
     * @param {boolean} anExpression
     */
    TestCase.prototype.assertFalse = function(anExpression)
    {
	"use strict";
	if(anExpression)
	{
	    throw new Error('excepted false');
	}
    };
    
    TestCase.prototype.assertEquals = function(value1, value2)
    {
	"use strict";
	if(value1 != value2)
	{
	    throw new Error(value1 +' != '+ value2);
	}
    };
    
    TestCase.prototype.testAll = function()
    {
	"use strict";
	var methodNames = getNamesOfMethodsOfObject(this);
	var methodTestNames = methodNames.filter(function (name) {
	    return StringUtils.startsWith(name, 'test') && name != 'testAll';
	});
	for(var index = 0; index < methodTestNames.length; ++index)
	{
	    var methodTestName = methodTestNames[index];
	    try
	    {
	        this[methodTestName]();
	    }
	    catch(exception)
	    {
	        if(typeof(window.console)       == 'object' &&
	           typeof(window.console.error) == 'function')
	        {
		    window.console.error('Test: '+ exception);
	        }
	    }
	}
    };
    
    
    /**
     * @constructor
     * @extends {TestCase}
     */
    function StringUtilsTest()
    {
	"use strict";
	TestCase.call(this);
    }
    classExtends(StringUtilsTest, TestCase);
    
    StringUtilsTest.prototype.testIsEmpty = function()
    {
	"use strict";
	this.assertTrue(StringUtils.isEmpty(''));
	this.assertFalse(StringUtils.isEmpty('a'));
    };
    
    StringUtilsTest.prototype.testIsNotEmpty = function()
    {
	"use strict";
	this.assertTrue(StringUtils.isNotEmpty('a'));
	this.assertFalse(StringUtils.isNotEmpty(''));
    };
    
    StringUtilsTest.prototype.testStartsWith = function()
    {
	"use strict";
	this.assertTrue(StringUtils.startsWith('a', 'a'));
	this.assertTrue(StringUtils.startsWith('ab', 'ab'));
	this.assertTrue(StringUtils.startsWith('A text', 'A'));
	this.assertTrue(StringUtils.startsWith('A text', 'A '));
    };
    
    StringUtilsTest.prototype.testEndsWith = function()
    {
	"use strict";
	this.assertTrue(StringUtils.endsWith('a', 'a'));
	this.assertTrue(StringUtils.endsWith('ab', 'ab'));
	this.assertTrue(StringUtils.endsWith('A text', 'text'));
	this.assertTrue(StringUtils.endsWith('A text', ' text'));
    };
    
    StringUtilsTest.prototype.testContains = function()
    {
	"use strict";
	this.assertTrue(StringUtils.contains('a', 'a'));
	this.assertTrue(StringUtils.contains('ab', 'a'));
	this.assertTrue(StringUtils.contains('ba', 'a'));
	this.assertTrue(StringUtils.contains('abc', 'b'));
	this.assertTrue(StringUtils.contains('GNU project', 'GNU'));
	this.assertTrue(StringUtils.contains('GNU project', 'project'));
	this.assertTrue(StringUtils.contains('Richard Stallman', 'Stallman'));
	this.assertFalse(StringUtils.contains('GNU', 'Linux'));
	this.assertFalse(StringUtils.contains('Linux', 'GNU'));
    };
    
    
    /**
     * @constructor
     * @extends {TestCase}
     */
    function ScriptSrcDynamicProxifierTest()
    {
	"use strict";
	TestCase.call(this);
    }
    classExtends(ScriptSrcDynamicProxifierTest, TestCase);
    
    ScriptSrcDynamicProxifierTest.prototype.testProxify = function()
    {
	"use strict";
	this.assertTrue(typeof(document.createElement) == 'function');
	var element = document.createElement('script');
	this.assertTrue(typeof(element.setAttribute) == 'function');
	element.setAttribute('src', 'script.js');
	this.assertTrue(typeof(element.getAttribute) == 'function');
	var value = element.getAttribute('src');
	this.assertFalse(StringUtils.startsWith(value, 'script.js'));
	this.assertTrue(StringUtils.contains(value, 'script.js'));
	this.assertTrue(value.length > 'script.js'.length);
    };
    
    
    var tests = [
        new StringUtilsTest(),
        new ScriptSrcDynamicProxifierTest()
    ];
    for(var index = 0; index < tests.length; ++index)
    {
	var currentTest = tests[index];
	currentTest.testAll();
    }
})();
