/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedPauseInteger = {};

/** @const */
WebSessionCapturedPauseInteger.XPATH_EXPRESSION_ROOT = (
    "//pause[string(number(@value)) != 'NaN']"
);
/** @const */
WebSessionCapturedPauseInteger.XPATH_EXPRESSION_NODE = (
    "."+ WebSessionCapturedPauseInteger.XPATH_EXPRESSION_ROOT
);

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseInteger.manageNodeBasic =
    function(node)
{
    "use strict";
    
    if(!WebSessionCapturedPauseTransformGeneric.manageNodeBasic(node))
    {
	return false;
    }
    
    var valueString = node.getAttribute('value');
    var valueInteger = parseInt(valueString, 10);
    if(isNaN(valueInteger) || valueInteger < 0)
    {
	return false;
    }
    
    valueInteger = Math.round(valueInteger);
    node.setAttribute('value', valueInteger);
    return true;
};

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseInteger.manageNodeWithXPath =
    function(node)
{
    "use strict";
    return WebSessionCapturedPauseTransformGeneric.manageNodeWithXPath(
        node,
        WebSessionCapturedPauseInteger.XPATH_EXPRESSION_NODE,
        WebSessionCapturedPauseInteger.manageNodeBasic
    );
};

/**
 * @param {Object} node
 * @return {boolean}
 */
WebSessionCapturedPauseInteger.manageNode = (
    WebSessionCapturedPauseInteger.manageNodeWithXPath
);

/**
 * @param {Object} document
 * @return {boolean}
 */
WebSessionCapturedPauseInteger.convertDomDocument =
    function(document)
{
    "use strict";
    return WebSessionCapturedPauseTransformGeneric.convertDomDocument(
        document, WebSessionCapturedPauseInteger.manageNode
    );
};

/**
 * @param {Object} docSource
 * @return {Object}
 */
WebSessionCapturedPauseInteger.convertDomDocToDomDocument =
    function(docSource)
{
    "use strict";
    var docResult = domDocumentClone(docSource);
    WebSessionCapturedPauseInteger.convertDomDocument(docResult);
    return docResult;
};
