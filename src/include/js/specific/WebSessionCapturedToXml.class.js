/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var WebSessionCapturedToXml = {};

WebSessionCapturedToXml.convertDomDocToDomDocWithXsltProcessor =
    function(docSource, xsltProcessor)
{
    "use strict";
    if(typeof(xsltProcessor) != 'object' ||
       typeof(xsltProcessor.transformToDocument) != 'function')
    {
        return null;
    }
    var docResult = xsltProcessor.transformToDocument(docSource);
    return docResult;
};

/**
 * @param {String|string} xsltUrl
 * @return {Object}
 */
WebSessionCapturedToXml.convertDomDocToDomDocWithXsltUrl =
    function(docSource, xsltUrl)
{
    "use strict";
    var xsltProcessor = XsltProcessorCache.get(xsltUrl);
    var docRes = WebSessionCapturedToXml.convertDomDocToDomDocWithXsltProcessor(
        docSource, xsltProcessor
    );
    return docRes;
};

/**
 * @param {String|string} xmlStringSource
 * @return {Node}
 */
WebSessionCapturedToXml.convertXmlStringToDomDocument =
    function(xmlStringSource, functionDomDocToDomDoc)
{
    "use strict";
    if(typeof(DOMParser) != 'function' ||
       typeof(DOMParser.prototype.parseFromString) != 'function')
    {
	return null;
    }
    var aParser = new DOMParser();
    var docSource = aParser.parseFromString(
        xmlStringSource.toString(), "text/xml"
    );
    var docResult = functionDomDocToDomDoc(docSource);
    return docResult;
};

/**
 * @param {String|string} xmlStringSource
 * @return {String|string}
 */
WebSessionCapturedToXml.convertXmlStringToXmlString =
    function(xmlStringSource, functionDomDocToDomDoc)
{
    "use strict";
    
    if(xmlStringSource.length == 0)
    {
        return '';
    }
    
    var docResult = WebSessionCapturedToXml.convertXmlStringToDomDocument(
        xmlStringSource, functionDomDocToDomDoc
    );
    if(docResult == null)
    {
        return '';
    }
    
    var aSerializer = new XMLSerializer();
    var xmlStringResult = aSerializer.serializeToString(docResult);
    return xmlStringResult;
};
