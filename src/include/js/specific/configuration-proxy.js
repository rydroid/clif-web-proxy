/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @return {boolean}
 */
function setCookieForProxyUrl()
{
    'use strict';
    var proxyUrlElement = document.getElementById('proxy-url');
    if(proxyUrlElement)
    {
	var proxyUrlValue = proxyUrlElement.value;
	if(proxyUrlValue.trim().length > 0)
	{
	    if(getCookie('proxy-url') != proxyUrlValue)
	    {
		setCookie('proxy-url', proxyUrlValue);
		updateHtmlConfiguration();
	    }
	    return true;
	}
    }
    return false;
}

/**
 * @return {boolean}
 */
function setCookieForProxyPort()
{
    'use strict';
    var proxyPortElement = document.getElementById('proxy-port');
    if(proxyPortElement)
    {
	var proxyPortValue = proxyPortElement.value;
	proxyPortValue = proxyPortValue.trim();
	if(proxyPortValue.length > 0 &&
	   !isNaN(proxyPortValue) &&
	   parseInt(proxyPortValue, 10) >= 0)
	{
	    var currentProxyPortCookieValue = getCookie('proxy-port');
	    currentProxyPortCookieValue = currentProxyPortCookieValue.trim();
	    if(currentProxyPortCookieValue != proxyPortValue)
	    {
		setCookie('proxy-port', proxyPortValue);
		updateHtmlConfiguration();
	    }
	    return true;
	}
    }
    return false;
}

/**
 * @return {number}
 */
function removeCookiesForProxy()
{
    'use strict';
    var nbRemoved = 0;
    if(removeCookie('proxy-url'))
    {
	++nbRemoved;
    }
    if(removeCookie('proxy-URL'))
    {
	++nbRemoved;
    }
    if(removeCookie('proxy-port'))
    {
	++nbRemoved;
    }
    if(removeCookie('proxy-port-nb'))
    {
	++nbRemoved;
    }
    if(removeCookie('proxy-port-number'))
    {
	++nbRemoved;
    }
    return nbRemoved;
}

/**
 * @return {boolean}
 */
function clearUrlOfFormForProxyConfiguration()
{
    'use strict';
    var element = document.getElementById('proxy-url');
    if(element)
    {
	element.value = '';
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function clearPortOfFormForProxyConfiguration()
{
    'use strict';
    var element = document.getElementById('proxy-port');
    if(element)
    {
	element.value = '';
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function clearFieldsOfFormForProxyConfiguration()
{
    'use strict';
    var result = true;
    result &= clearUrlOfFormForProxyConfiguration();
    result &= clearPortOfFormForProxyConfiguration();
    return result;
}

/**
 * @return {boolean}
 */
function onSubmitOfFormForProxyConfiguration()
{
    'use strict';
    setCookieForProxyUrl();
    setCookieForProxyPort();
    return false;
}

/**
 * @return {boolean}
 */
function onResetOfFormForProxyConfiguration()
{
    'use strict';
    if(removeCookiesForProxy() > 0)
    {
	updateHtmlConfiguration();
    }
    return clearFieldsOfFormForProxyConfiguration();
}

/**
 * @return {boolean}
 */
function addEventListenersForProxyConfiguration()
{
    'use strict';
    
    var formElement = null;
    if(typeof(document.getElementById) == 'function')
    {
	formElement = document.getElementById('cookies-proxy-form');
    }
    else if(typeof(document.querySelector) == 'function')
    {
	formElement = document.querySelector('#cookies-proxy-form');
    }
    if(formElement == null)
    {
	return false;
    }
    
    formElement.onsubmit = onSubmitOfFormForProxyConfiguration;
    formElement.onreset  = onResetOfFormForProxyConfiguration;
    return true;
}

/**
 * @return {boolean}
 */
function onProxyConfigurationPageLoaded()
{
    'use strict';
    return addEventListenersForProxyConfiguration();
}
