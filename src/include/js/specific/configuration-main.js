/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @param {String|string}    cookieName
 * @param {(String|string)=} elementId
 * @param {Object=}          event
 */
function onEventOnCheckboxToSaveValueInCookie(cookieName, elementId, event)
{
    'use strict';
    var element = null;
    if(typeof(event) == 'object' && typeof(event.currentTarget) == 'object')
    {
	element = event.currentTarget;
    }
    else if(typeof(document.getElementById) == 'function' &&
	    typeof(elementId) != 'undefined' &&
	    elementId.toString().trim().length > 0)
    {
	element = document.getElementById(elementId.toString());
    }
    else
    {
	return;
    }
    if(typeof(element) == 'object' &&
       typeof(element.checked) != 'undefined' &&
       getCookie(cookieName) != element.checked)
    {
	setCookie(cookieName, element.checked);
	updateHtmlConfiguration();
    }
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save', 'session-save', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveProxyCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-proxy', 'session-save-proxy', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveCookiesCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-cookies', 'session-save-cookies', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveUserAgentCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-user-agent', 'session-save-user-agent', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveRefererCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-referer', 'session-save-referer', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveLanguageCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
	'session-save-language', 'session-save-language', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveEncodingCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-encoding', 'session-save-encoding', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveEtagCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-etag', 'session-save-etag', event
    );
}

/**
 * @param {Object=} event
 */
function onEventOnSessionCapturedSaveDoNotTrackCheckbox(event)
{
    'use strict';
    onEventOnCheckboxToSaveValueInCookie(
        'session-save-do-not-track', 'session-save-do-not-track', event
    );
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSave()
{
    'use strict';
    var element = document.getElementById('session-save');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveProxy()
{
    'use strict';
    var element = document.getElementById('session-save-proxy');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveProxyCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveCookies()
{
    'use strict';
    var element = document.getElementById('session-save-cookies');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveCookiesCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveUserAgent()
{
    'use strict';
    var element = document.getElementById('session-save-user-agent');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveUserAgentCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveReferer()
{
    'use strict';
    var element = document.getElementById('session-save-referer');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveRefererCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveLanguage()
{
    'use strict';
    var element = document.getElementById('session-save-language');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveLanguageCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveEncoding()
{
    'use strict';
    var element = document.getElementById('session-save-encoding');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveEncodingCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveEtag()
{
    'use strict';
    var element = document.getElementById('session-save-etag');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveEtagCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenerForSessionCapturedConfigurationSaveDoNotTrack()
{
    'use strict';
    var element = document.getElementById('session-save-dnt');
    if(element)
    {
	element.addEventListener(
	    'change', onEventOnSessionCapturedSaveDoNotTrackCheckbox, false
	);
	return true;
    }
    return false;
}

/**
 * @return {boolean}
 */
function addEventListenersForSessionCapturedConfigurationSaveHttpParts()
{
    'use strict';
    /** @type {boolean} */
    var result = true;
    result &= addEventListenerForSessionCapturedConfigurationSaveCookies();
    result &= addEventListenerForSessionCapturedConfigurationSaveUserAgent();
    result &= addEventListenerForSessionCapturedConfigurationSaveReferer();
    result &= addEventListenerForSessionCapturedConfigurationSaveLanguage();
    result &= addEventListenerForSessionCapturedConfigurationSaveEncoding();
    result &= addEventListenerForSessionCapturedConfigurationSaveEtag();
    result &= addEventListenerForSessionCapturedConfigurationSaveDoNotTrack();
    return result;
}

/**
 * @return {boolean}
 */
function addEventListenersForSessionCapturedConfiguration()
{
    'use strict';
    /** @type {boolean} */
    var result = true;
    result &= addEventListenerForSessionCapturedConfigurationSave();
    result &= addEventListenerForSessionCapturedConfigurationSaveProxy();
    result &= addEventListenersForSessionCapturedConfigurationSaveHttpParts();
    return result;
}

/**
 * @return {boolean}
 */
function onSessionCapturedConfigurationPageLoaded()
{
    'use strict';
    var submitElement = document.getElementById('cookies-session-submit');
    if(typeof(submitElement) == 'object')
    {
	submitElement.hidden = true;
    }
    return addEventListenersForSessionCapturedConfiguration();
}


/**
 * @return {boolean}
 */
function onConfigurationPageLoaded()
{
    'use strict';
    /** @type {boolean} */
    var result = true;
    result &= onProxyConfigurationPageLoaded();
    result &= onSessionCapturedConfigurationPageLoaded();
    return result;
}

if(typeof(window.addEventListener) == 'function')
{
    window.addEventListener('load', onConfigurationPageLoaded, false);
}
else
{
    window.onload = onConfigurationPageLoaded;
}
