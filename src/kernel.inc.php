<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);

if(function_exists('mb_internal_encoding'))
{
    mb_internal_encoding('UTF-8');
}

require_once('autoload.inc.php');

# Some usefull variables
define('DOMAIN_NAME', ServerUtils::getName());
define('URL', ServerUtils::getUrlWithoutFile());

set_error_handler(
    function(int $errno, string $errstr,
             string $errfile, int $errline, array $errcontext)
    {
        # error was suppressed with the @-operator
        if(0 === error_reporting())
        {
            return false;
        }
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }
);
