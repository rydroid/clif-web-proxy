# Free/libre web capture proxy

This project aims to make it easy to capture a web "session",
in order to be complementary for tools that tests network behaviour/load of web apps
(like [CLIF](https://clif.ow2.io/)).
It aims to make capture and replay of web sessions easier,
however it could be used as a simple web proxy.

It is [free/libre software](https://www.gnu.org/philosophy/free-sw).
It does not depend on [proprietary software](https://www.gnu.org/proprietary/).
Happy [hacking](https://stallman.org/articles/on-hacking.html)!

## Tools

### Needed tools

- Web server (like Apache or nginx)
  - Apache module for PHP
- [PHP](https://secure.php.net/) 7
- [PHP Multibyte String](https://secure.php.net/manual/en/book.mbstring.php)
- [PHP XML](https://secure.php.net/manual/en/book.xml.php)
- [Pandoc](https://pandoc.org/)
  (to generate documentation in HTML from markdown)

### Optionnal tools

#### Recommanded tool

- [PHP cURL](https://secure.php.net/manual/en/book.curl.php)
  (to catch HTTP headers)

#### Testing tools (for developers)

- Static analysis
  - Generic
    - [KWStyle](https://github.com/Kitware/KWStyle)
    - [vera++](https://bitbucket.org/verateam/vera/wiki/Home)
  - PHP
    - [PHPMD (PHP Mess Detector)](https://phpmd.org/)
    - [PHPCDP (PHP Copy/Paste Detector)](https://github.com/sebastianbergmann/phpcpd)
    - [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
  - JavaScript
    - [Closure Compiler](https://developers.google.com/closure/compiler/)
      (mostly checks syntax and types)
  - [XML (eXtensible Markup Language)](https://www.w3.org/TR/REC-xml/)
    - [xmllint](http://xmlsoft.org/xmllint.html)
  - [YAML (YAML Ain't Markup Language)](http://yaml.org/)
    - [yamllint](https://github.com/adrienverge/yamllint)
- Dynamic analysis
  - PHP
    - [PHPUnit](https://phpunit.de/) 5
  - XML
    - [xsltproc](http://xmlsoft.org/XSLT/)
    - [Xalan](https://xalan.apache.org/)
    - [Saxon XSLT](https://en.wikipedia.org/wiki/Saxon_XSLT)
      (free/libre versions only)
- Continuous Integration
  - [Jenkins](https://jenkins.io/)
    - [Travis YML Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Travis+YML+Plugin)
  - [GitLab CI](https://docs.gitlab.com/ce/ci/)

#### Other tools

- [Doxygen](https://www.stack.nl/~dimitri/doxygen/) (to generate documentation)
  - [Graphviz](http://graphviz.org/) (to generate graphs for the documentation)
- [git](https://git-scm.com/) (for the history of the project)
- [make](https://en.wikipedia.org/wiki/Make_(software))
- [Composer](https://getcomposer.org/) (mostly a wrapper for make)

### Install them

There are rules in the makefile for that.
The rules starts by `install-`.
