# Changelog

For a quick summary, there is the file `debian/changelog`.

For a full "picture", there is the git repository.
It is publicly available (at least) on:

- [GitLab instance of OW2](https://gitlab.ow2.org/rydroid/clif-web-proxy)
- [GitLab.com](https://gitlab.com/RyDroid/proxy-php)
