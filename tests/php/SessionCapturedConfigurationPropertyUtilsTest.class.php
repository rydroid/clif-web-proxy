<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class SessionCapturedConfigurationPropertyUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetSaveFromMapWithoutSection()
    {
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getSaveFromMapWithoutSection(array()),
            ''
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getSaveFromMapWithoutSection(
                array('save-session-captured' => 'true')
            ),
            'true'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getSaveFromMapWithoutSection(
                array('save-session' => 'true')
            ),
            'true'
        );
    }
    
    public function
        testGetDirectoryFromMapWithoutSection()
    {
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getDirectoryFromMapWithoutSection(array()),
            ''
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getDirectoryFromMapWithoutSection(
                array('' => '/tmp')
            ),
            ''
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getDirectoryFromMapWithoutSection(
                array('session-captured-directory' => '/tmp')
            ),
            '/tmp'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getDirectoryFromMapWithoutSection(
                array('session-captured-dir' => '/tmp')
            ),
            '/tmp'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getDirectoryFromMapWithoutSection(
                array('session-captured-folder' => '/tmp')
            ),
            '/tmp'
        );
    }
    
    public function
        testGetCreateAutomaticallyFromMapWithoutSection()
    {
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection(array()),
            ''
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection(
                array('session-create-auto' => 'false')
            ),
            'false'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection(
                array('session-create-auto' => 'no')
            ),
            'no'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection(
                array('session-create-auto' => 'true')
            ),
            'true'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapWithoutSection(
                array('session-create-auto' => 'yes')
            ),
            'yes'
        );
    }
    
    public function
        testGetCreateAutomaticallyFromMapInSection()
    {
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapInSection(array()),
            ''
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapInSection(
                array('create-auto' => 'false')
            ),
            'false'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapInSection(
                array('create-auto' => 'no')
            ),
            'no'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapInSection(
                array('create-auto' => 'true')
            ),
            'true'
        );
        
        $this->assertEquals(
            SessionCapturedConfigurationPropertyUtils::
            getCreateAutomaticallyFromMapInSection(
                array('create-auto' => 'yes')
            ),
            'yes'
        );
    }
}
