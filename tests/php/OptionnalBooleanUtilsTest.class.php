<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class OptionnalBooleanUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testIsValidInteger()
    {
        $this->assertTrue(
            OptionnalBooleanUtils::isValidInteger(
                OptionnalBooleanConst::UNDEFINED
            )
        );
        $this->assertTrue(
            OptionnalBooleanUtils::isValidInteger(
                OptionnalBooleanConst::FALSE
            )
        );
        $this->assertTrue(
            OptionnalBooleanUtils::isValidInteger(
                OptionnalBooleanConst::TRUE
            )
        );
    }
    
    public function
        testIsValidString()
    {
        $this->assertFalse(OptionnalBooleanUtils::isValidString(''));
        $this->assertFalse(OptionnalBooleanUtils::isValidString('a'));
        $this->assertFalse(OptionnalBooleanUtils::isValidString('e'));
        $this->assertFalse(OptionnalBooleanUtils::isValidString('&'));
        $this->assertFalse(OptionnalBooleanUtils::isValidString('RMS'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('undefined'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('false'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('true'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString(' false '));
        $this->assertTrue(OptionnalBooleanUtils::isValidString(' true '));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('no'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('yes'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString(' no '));
        $this->assertTrue(OptionnalBooleanUtils::isValidString(' yes '));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('non'));
        $this->assertTrue(OptionnalBooleanUtils::isValidString('oui'));
    }
    
    public function
        testCreateUndefined()
    {
        $value = OptionnalBooleanUtils::createUndefined();
        $this->assertTrue($value->isUndefined());
        $this->assertFalse($value->isDefined());
        $this->assertFalse($value->isFalse());
        $this->assertFalse($value->isTrue());
    }
    
    public function
        testCreateFalse()
    {
        $value = OptionnalBooleanUtils::createFalse();
        $this->assertTrue($value->isFalse());
        $this->assertFalse($value->isTrue());
        $this->assertTrue($value->isDefined());
        $this->assertFalse($value->isUndefined());
    }
    
    public function
        testCreateTrue()
    {
        $value = OptionnalBooleanUtils::createTrue();
        $this->assertTrue($value->isTrue());
        $this->assertFalse($value->isFalse());
        $this->assertTrue($value->isDefined());
        $this->assertFalse($value->isUndefined());
    }
}
