<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlStringParseUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetWithoutFile()
    {
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile('http://domain.org'),
            'http://domain.org'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile('https://domain.org'),
            'https://domain.org'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile('http://domain.org:80'),
            'http://domain.org:80'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile('http://domain.org:80/'),
            'http://domain.org:80/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile(
                'http://domain.org/test/img.jpg'
            ),
            'http://domain.org/test/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile(
                'http://domain.org:80/test/img.jpg'
            ),
            'http://domain.org:80/test/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile(
                'https://domain.org:80/test/img.jpg'
            ),
            'https://domain.org:80/test/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile('/test/img.jpg'),
            '/test/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutFile('./test/img.jpg'),
            './test/'
        );
    }
    
    public function
        testGetDirectory()
    {
        $this->assertEquals(
            UrlStringParseUtils::getDirectory('http://domain.org/img.jpg'),
            ''
        );
        $this->assertEquals(
            UrlStringParseUtils::getDirectory('http://domain.org/test/img.jpg'),
            'test'
        );
        $this->assertEquals(
            UrlStringParseUtils::getDirectory('/test/img.jpg'),
            '/test'
        );
        $this->assertEquals(
            UrlStringParseUtils::getDirectory('./test/img.jpg'),
            'test'
        );
    }
    
    public function
        testGetFileName()
    {
        $this->assertEquals(
            UrlStringParseUtils::getFileName('http://domain.org/img.jpg'),
            'img.jpg'
        );
        $this->assertEquals(
            UrlStringParseUtils::getFileName('https://domain.org/img.jpg'),
            'img.jpg'
        );
        $this->assertEquals(
            UrlStringParseUtils::getFileName('https://domain.org/test/img.jpg'),
            'img.jpg'
        );
    }

    public function
        testGetWithoutDirectoryAndFileName()
    {
        $this->assertEquals(
            UrlStringParseUtils::getWithoutDirectoryAndFileName(
                'https://domain.org/img.jpg'
            ),
            'https://domain.org/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutDirectoryAndFileName(
                'https://domain.org/test/img.jpg'
            ),
            'https://domain.org/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutDirectoryAndFileName(
                'https://user@domain.org/img.jpg'
            ),
            'https://user@domain.org/'
        );
        $this->assertEquals(
            UrlStringParseUtils::getWithoutDirectoryAndFileName(
                'https://user@domain.org:80/img.jpg'
            ),
            'https://user@domain.org:80/'
        );
    }
}
