<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testConcatenate()
    {
        $this->assertEquals(
            UrlStringUtils::concatenate('', ''),
            ''
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('', 'a'),
            'a'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('', 'b'),
            'b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a', ''),
            'a'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('b', ''),
            'b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a', 'b'),
            'a/b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a/', 'b'),
            'a/b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a', '/b'),
            'a/b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a/', '/b'),
            'a/b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a/', './b'),
            'a/b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('./a/', './b'),
            './a/b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('a/', '../b'),
            'a/../b'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('server.net', 'ressource'),
            'server.net/ressource'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('server.net/', 'ressource'),
            'server.net/ressource'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('server.net', '/ressource'),
            'server.net/ressource'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('server.net/', '/ressource'),
            'server.net/ressource'
        );
        $this->assertEquals(
            UrlStringUtils::concatenate('server.net:8080', 'ressource'),
            'server.net:8080/ressource'
        );
    }
    
    public function
        testComponentsToString()
    {
        $this->assertEquals(
            UrlStringUtils::componentsToString(
                array(
                    'scheme' => 'https',
                    'host'   => 'example.net',
                    'path'   => 'index.html'
                )
            ),
            'https://example.net/index.html'
        );
        
        $this->assertEquals(
            UrlStringUtils::componentsToString(
                array(
                    'scheme' => 'https',
                    'host'   => 'example.net',
                    'path'   => '/index.html'
                )
            ),
            'https://example.net/index.html'
        );
        
        $this->assertEquals(
            UrlStringUtils::componentsToString(
                array(
                    'scheme' => 'https',
                    'host'   => 'example.net/',
                    'path'   => 'index.html'
                )
            ),
            'https://example.net/index.html'
        );
        
        $this->assertEquals(
            UrlStringUtils::componentsToString(
                array(
                    'scheme' => 'https',
                    'host'   => 'example.net/',
                    'path'   => '/index.html'
                )
            ),
            'https://example.net/index.html'
        );
    }
    
    public function
        testAddStringParameters()
    {
        $this->assertEquals(
            UrlStringUtils::addStringParameters(
                'https://example.net/',
                'param=value'
            ),
            'https://example.net/?param=value'
        );
        $this->assertEquals(
            UrlStringUtils::addStringParameters(
                'https://example.net/',
                'param1=value1&param2=value2'
            ),
            'https://example.net/?param1=value1&param2=value2'
        );
        $this->assertEquals(
            UrlStringUtils::addStringParameters(
                'https://example.net/?key=value',
                'param1=value1&param2=value2'
            ),
            'https://example.net/?key=value&param1=value1&param2=value2'
        );
    }
    
    public function
        testRemoveStringParameters()
    {
        $this->assertEquals(
            UrlStringUtils::removeStringParameters(
                'https://example.net/',
                'param=value'
            ),
            'https://example.net/'
        );
        $this->assertEquals(
            UrlStringUtils::removeStringParameters(
                'https://example.net/?param=value',
                'param=value'
            ),
            'https://example.net/'
        );
        $this->assertEquals(
            UrlStringUtils::removeStringParameters(
                'https://example.net/?param1=value1&param2=value2',
                'param1=value1'
            ),
            'https://example.net/?param2=value2'
        );
        $this->assertEquals(
            UrlStringUtils::removeStringParameters(
                'https://example.net/?param1=value1&param2=value2',
                'param2=value2'
            ),
            'https://example.net/?param1=value1'
        );
    }
}
