<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierSourceSrcTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttributeOne(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            $html,
            new DomDocumentImportExport()
        );
    }
    
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio><source src="" /></audio></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio><source src=" " /></audio></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio>'.
            '<source src="" type="audio/ogg" />'
            .'</audio></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio>'.
            '<source src="" type="audio/mpeg" />'
            .'</audio></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><video><source src="" /></video></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><video><source src=" " /></video></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><picture><source src="" /></picture></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><picture><source src=" " /></picture></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><source src="" /></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><source src=" " /></body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttributeOne(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            $html,
            '/html/body//*/source[string(@src)]',
            'src',
            new DomDocumentImportExport()
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio>'.
            '<source src="song.oga" />'
            .'</audio></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio>'.
            '<source src="song.oga" type="audio/ogg" />'
            .'</audio></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio>'.
            '<source src="song.mp3" />'
            .'</audio></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><audio>'.
            '<source src="song.mp3" type="audio/mpeg" />'
            .'</audio></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><video>'.
            '<source src="video.ogv" />'
            .'</video></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><video>'.
            '<source src="video.webm" />'
            .'</video></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><video>'.
            '<source src="video.mp4" />'
            .'</video></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><picture>'.
            '<source src="img.svg" />'
            .'</picture></body></html>'
        );
        self::proxifyDocumentWithFilledAttributeOne(
            $testCase,
            $proxifier,
            '<html><body><picture>'.
            '<source src="img.svg" type="image/svg+xml" />'
            .'</picture></body></html>'
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierSourceSrc();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
