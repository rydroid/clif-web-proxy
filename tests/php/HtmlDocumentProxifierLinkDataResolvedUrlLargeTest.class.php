<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierLinkDataResolvedUrlLargeTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<a data-resolved-url-large="">A link</a>'
            .'</body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<a data-resolved-url-large="index.html">Link</a>'
            .'</body></html>',
            '/html/body//a[string(@data-resolved-url-large)]',
            'data-resolved-url-large'
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierLinkDataResolvedUrlLarge();
    }
    
    
    public function
        getProxifier() : DomDocumentProxifierAbstract
    {
        return $this->proxifier;
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->getProxifier()
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $this,
            $this->getProxifier(),
            '<html><body>'.
            '<a href="index.html" data-resolved-url-large="">A link</a>'
            .'</body></html>'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $this,
            $this->getProxifier(),
            '<html><body>'.
            '<a data-url="index.html" data-resolved-url-large="">A link</a>'
            .'</body></html>'
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->getProxifier()
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $this,
            $this->getProxifier(),
            '<html><body>'.
            '<a href="index.html" data-resolved-url-large="index.html">Link</a>'
            .'</body></html>',
            '/html/body//a[string(@data-resolved-url-large)]',
            'data-resolved-url-large'
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $this,
            $this->getProxifier(),
            '<html><body>'.
            '<a data-url="page.htm" data-resolved-url-large="page.htm">Link</a>'
            .'</body></html>',
            '/html/body//a[string(@data-resolved-url-large)]',
            'data-resolved-url-large'
        );
    }
}
