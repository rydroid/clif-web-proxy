<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierFormActionTest
    extends PHPUnit_Framework_TestCase
{
    /**
     * @SuppressWarnings(BooleanArgumentFlag)
     */
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            bool $nbElementsEqual = true
        )
    {
        $nbElements = $nbElementsEqual ? 1 : -1;
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action=""></form></body></html>',
            $nbElements
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action="" method="GET"></form></body></html>',
            $nbElements
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action="" method="POST"></form></body></html>',
            $nbElements
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action="" method="GET">'.
            '<input type="text" /><input type="submit" />'
            .'</form></body></html>',
            $nbElements
        );
        DomDocumentProxifierAttributeTest::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action="" method="GET">'.
            '<label for="name">Your name</label>'.
            '<input type="text" id="name" />'.
            '<input type="submit" />'.
            '</form></body></html>',
            '/html/body//form[string(@action)]',
            'action',
            $nbElements
        );
    }
    
    /**
     * @SuppressWarnings(BooleanArgumentFlag)
     */
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            bool $nbElementsEqual = true
        )
    {
        $nbElements = $nbElementsEqual ? 1 : -1;
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action="compute.php" method="GET">'.
            '<input type="text" /><input type="submit" />'
            .'</form></body></html>',
            '/html/body//form[string(@action)]',
            'action',
            $nbElements
        );
        
        DomDocumentProxifierAttributeTest::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><form action="compute.php" method="GET">'.
            '<label for="name">Your name</label>'.
            '<input type="text" id="name" />'.
            '<input type="submit" />'.
            '</form></body></html>',
            '/html/body//form[string(@action)]',
            'action',
            $nbElements
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierFormAction();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
