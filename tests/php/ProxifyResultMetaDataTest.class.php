<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class ProxifyResultMetaDataTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testUrl()
    {
        $data = new ProxifyResultMetaData('');
        $this->assertEquals($data->getUrl(), '');
        $this->assertFalse($data->hasUrl());
        
        $data = new ProxifyResultMetaData('https://example.info');
        $this->assertEquals($data->getUrl(), 'https://example.info');
        $this->assertTrue($data->hasUrl());
    }
    
    public function
        testTimeRequest()
    {
        $data = new ProxifyResultMetaData('https://example.info');
        
        $this->assertFalse($data->setTimeRequestMicroSecond(-1));
        $this->assertFalse($data->setTimeRequestMilliSecond(-1));
        $this->assertTrue($data->setTimeRequestMicroSecond(1));
        $this->assertTrue($data->setTimeRequestMilliSecond(1));
        
        $this->assertTrue($data->setTimeRequestMilliSecond(1));
        $this->assertEquals($data->getTimeRequestMilliSecond(), 1);
        $this->assertEquals($data->getTimeRequestMicroSecond(), 1000);
        
        $this->assertTrue($data->setTimeRequestMicroSecond(1125));
        $this->assertEquals($data->getTimeRequestMicroSecond(), 1125);
        $this->assertEquals($data->getTimeRequestMilliSecond(), 1.125);
        
        $this->assertTrue($data->setTimeRequestToNow());
        $timeBefore = $data->getTimeRequestMilliSecond();
        usleep(5000);
        $this->assertTrue($data->setTimeRequestToNow());
        $timeAfter  = $data->getTimeRequestMilliSecond();
        $this->assertEquals((int)($timeAfter - $timeBefore), 5);
    }
    
    public function
        testTimeResponse()
    {
        $data = new ProxifyResultMetaData('https://example.info');
        
        $this->assertFalse($data->setTimeResponseMicroSecond(-1));
        $this->assertFalse($data->setTimeResponseMilliSecond(-1));
        $this->assertTrue($data->setTimeResponseMicroSecond(1));
        $this->assertTrue($data->setTimeResponseMilliSecond(1));
        
        $this->assertTrue($data->setTimeResponseMilliSecond(1));
        $this->assertEquals($data->getTimeResponseMilliSecond(), 1);
        $this->assertEquals($data->getTimeResponseMicroSecond(), 1000);
        
        $this->assertTrue($data->setTimeResponseMicroSecond(1125));
        $this->assertEquals($data->getTimeResponseMicroSecond(), 1125);
        $this->assertEquals($data->getTimeResponseMilliSecond(), 1.125);
        
        $this->assertTrue($data->setTimeResponseToNow());
        $timeBefore = $data->getTimeResponseMilliSecond();
        usleep(5000);
        $this->assertTrue($data->setTimeResponseToNow());
        $timeAfter  = $data->getTimeResponseMilliSecond();
        $this->assertEquals((int)($timeAfter - $timeBefore), 5);
    }
    
    public function
        testTimeRequestResponse()
    {
        $data = new ProxifyResultMetaData('https://example.info');
        $this->assertTrue($data->setTimeRequestMilliSecond(1));
        $this->assertTrue($data->setTimeResponseMilliSecond(2));
        $this->assertEquals($data->getTimeRequestMilliSecond(), 1);
        $this->assertEquals($data->getTimeResponseMilliSecond(), 2);
    }
    
    public function
        testHeadersClient()
    {
        $data = new ProxifyResultMetaData('https://example.info');
        $this->assertFalse($data->hasHeadersClient());
    }
}
