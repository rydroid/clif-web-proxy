<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class DomDocumentProxifierAttributeUtils
{
    public static function
        proxifyDocumentWithAttributeToKeep(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html,
            DomDocumentImportExport $docLoadSave
        )
    {
        $document = $docLoadSave->getDomDocument();
        $testCase->assertTrue($docLoadSave->loadFromText($html));
        $htmlSavedBefore = trim($docLoadSave->saveToText());
        $testCase->assertEquals($proxifier->proxifyDocument($document), 0);
        $htmlSavedAfter  = trim($docLoadSave->saveToText());
        $testCase->assertEquals($htmlSavedBefore, $htmlSavedAfter);
    }
    
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html,
            DomDocumentImportExport $docLoadSave
        )
    {
        self::proxifyDocumentWithAttributeToKeep(
            $testCase, $proxifier,
            $html, $docLoadSave
        );
    }
    
    /**
     * @SuppressWarnings(ElseExpression)
     */
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html,
            string $xpathQuery,
            string $attribute,
            DomDocumentImportExport $docLoadSave,
            int $nbElements = 1
        )
    {
        $document = $docLoadSave->getDomDocument();
        
        $testCase->assertTrue($docLoadSave->loadFromText($html));
        $htmlSavedBefore = trim($docLoadSave->saveToText());
        $nbModified = $proxifier->proxifyDocument($document);
        if($nbElements < 0)
        {
            $testCase->assertLessThanOrEqual(
                $nbModified, - $nbElements
            );
        }
        else
        {
            $testCase->assertEquals($nbModified, $nbElements);
        }
        $htmlSavedAfter  = trim($docLoadSave->saveToText());
        $testCase->assertNotEquals($htmlSavedBefore, $htmlSavedAfter);
        
        $testCase->assertTrue($docLoadSave->loadFromText($htmlSavedAfter));
        $xpath = new DOMXPath($document);
        $elements = $xpath->query($xpathQuery);
        
        if($nbElements < 0)
        {
            $testCase->assertLessThanOrEqual(
                $elements->length, - $nbElements
            );
        }
        else
        {
            $testCase->assertEquals($elements->length, $nbElements);
        }

        $defaultUrl = UrlProxifierDefaultUtils::getUrl();
        foreach($elements as $element)
        {
            $testCase->assertTrue($element->hasAttribute($attribute));
            $url = $element->getAttribute($attribute);
            $testCase->assertNotEmpty($url);
            if($url !=
               HtmlDocumentProxifierJavaScriptDynamic::SCRIPT_FILE_PATH)
            {
                $testCase->assertStringStartsWith($defaultUrl, $url);
            }
        }
    }
}
