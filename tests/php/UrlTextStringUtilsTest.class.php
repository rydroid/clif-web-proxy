<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlTextStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testIsXml()
    {
        $this->assertTrue(UrlTextStringUtils::isXml('document.xml'));
        $this->assertTrue(UrlTextStringUtils::isXml('page.xhtml'));
        $this->assertTrue(UrlTextStringUtils::isXml('image.svg'));
        $this->assertTrue(UrlTextStringUtils::isXml('data/document.xml'));
        $this->assertTrue(
            UrlTextStringUtils::isXml(
                'https://example.org/data/document.xml'
            )
        );
    }
    
    public function
        testIsNotXml()
    {
        $this->assertFalse(UrlTextStringUtils::isXml(''));
        $this->assertFalse(UrlTextStringUtils::isXml('.'));
        $this->assertFalse(UrlTextStringUtils::isXml('..'));
        $this->assertFalse(UrlTextStringUtils::isXml('page.html'));
        $this->assertFalse(UrlTextStringUtils::isXml('page.htm'));
        $this->assertFalse(UrlTextStringUtils::isXml('readme.txt'));
    }
    
    public function
        testIsJson()
    {
        $this->assertTrue(UrlTextStringUtils::isJson('data.json'));
        $this->assertFalse(UrlTextStringUtils::isJson('data.xml'));
        $this->assertFalse(UrlTextStringUtils::isJson('page.xhtml'));
        $this->assertFalse(UrlTextStringUtils::isJson('script.js'));
        $this->assertFalse(UrlTextStringUtils::isJson('..'));
        $this->assertFalse(UrlTextStringUtils::isJson('.'));
        $this->assertFalse(UrlTextStringUtils::isJson('/'));
        $this->assertFalse(UrlTextStringUtils::isJson(''));
    }
    
    public function
        testIsHtml()
    {
        $this->assertTrue(UrlTextStringUtils::isHtml('page.html'));
        $this->assertTrue(UrlTextStringUtils::isHtml('page.HTML'));
        $this->assertTrue(UrlTextStringUtils::isHtml('page.xhtml'));
        $this->assertTrue(UrlTextStringUtils::isHtml('page.XHTML'));
        $this->assertTrue(UrlTextStringUtils::isHtml('page.htm'));
        $this->assertTrue(UrlTextStringUtils::isHtml('page.HTM'));
        $this->assertTrue(UrlTextStringUtils::isHtml('html/page.html'));
        $this->assertTrue(UrlTextStringUtils::isHtml('web/page.html'));
        $this->assertTrue(
            UrlTextStringUtils::isHtml(
                'https://example.org/index.html'
            )
        );
    }
    
    public function
        testIsNotHtml()
    {
        $this->assertFalse(UrlTextStringUtils::isHtml(''));
        $this->assertFalse(UrlTextStringUtils::isHtml('.'));
        $this->assertFalse(UrlTextStringUtils::isHtml('..'));
        $this->assertFalse(UrlTextStringUtils::isHtml('/'));
        $this->assertFalse(UrlTextStringUtils::isHtml('data.xml'));
        $this->assertFalse(UrlTextStringUtils::isHtml('html.xml'));
        $this->assertFalse(UrlTextStringUtils::isHtml('readme.txt'));
    }
    
    public function
        testIsCss()
    {
        $this->assertTrue(UrlTextStringUtils::isCss('style.css'));
        $this->assertTrue(UrlTextStringUtils::isCss('design.css'));
        $this->assertTrue(UrlTextStringUtils::isCss('style.CSS'));
        $this->assertTrue(UrlTextStringUtils::isCss('css/style.css'));
        $this->assertTrue(UrlTextStringUtils::isCss('design/style.css'));
    }
    
    public function
        testIsNotCss()
    {
        $this->assertFalse(UrlTextStringUtils::isCss(''));
        $this->assertFalse(UrlTextStringUtils::isCss('.'));
        $this->assertFalse(UrlTextStringUtils::isCss('..'));
        $this->assertFalse(UrlTextStringUtils::isCss('/'));
        $this->assertFalse(UrlTextStringUtils::isCss('data.xml'));
        $this->assertFalse(UrlTextStringUtils::isCss('html.xml'));
        $this->assertFalse(UrlTextStringUtils::isCss('readme.txt'));
        $this->assertFalse(UrlTextStringUtils::isCss('page.html'));
        $this->assertFalse(UrlTextStringUtils::isCss('page.HTML'));
        $this->assertFalse(UrlTextStringUtils::isCss('page.xhtml'));
        $this->assertFalse(UrlTextStringUtils::isCss('page.XHTML'));
        $this->assertFalse(UrlTextStringUtils::isCss('page.htm'));
        $this->assertFalse(UrlTextStringUtils::isCss('page.HTM'));
        $this->assertFalse(UrlTextStringUtils::isCss('style.htm'));
        $this->assertFalse(UrlTextStringUtils::isCss('html/page.html'));
        $this->assertFalse(UrlTextStringUtils::isCss('web/page.html'));
        $this->assertFalse(
            UrlTextStringUtils::isCss(
                'https://example.org/index.html'
            )
        );
    }
}
