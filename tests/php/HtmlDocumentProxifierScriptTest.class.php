<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierScriptTest
    extends PHPUnit_Framework_TestCase
{
    /**
     * @SuppressWarnings(BooleanArgumentFlag)
     */
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            bool $nbElementsEqual = true
        )
    {
        $nbElements = $nbElementsEqual ? 1 : -1;
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><script src=""></script></body></html>',
            $nbElements
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<script src="" type="text/javascript"></script>'
            .'</body></html>',
            $nbElements
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head><script src=""></script></head></html>',
            $nbElements
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<script src="" type="text/javascript"></script>'.
            '</head></html>',
            $nbElements
        );
    }
    
    /**
     * @SuppressWarnings(BooleanArgumentFlag)
     */
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            bool $nbElementsEqual = true
        )
    {
        $nbElements = $nbElementsEqual ? 1 : -1;
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><script src="script.js"></script></body></html>',
            '/html//script[string(@src)]',
            'src',
            $nbElements
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<script src="script.js" type="text/javascript"></script>'
            .'</body></html>',
            '/html//script[string(@src)]',
            'src',
            $nbElements
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><head><script src="script.js"></script></head></html>',
            '/html//script[string(@src)]',
            'src',
            $nbElements
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<script src="script.js" type="text/javascript"></script>'.
            '</head></html>',
            '/html//script[string(@src)]',
            'src',
            $nbElements
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierScript();
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->proxifier
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->proxifier
        );
    }
}
