<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


abstract class DomDocumentProxifierWithXPathAbstractTest
    extends PHPUnit_Framework_TestCase
{
    private $proxifier;
    
    
    public function
        __construct(DomDocumentProxifierAbstract $proxifier)
    {
        $this->proxifier = $proxifier;
    }
    
    
    public function
        getProxifier() : DomDocumentProxifierAbstract
    {
        return $this->proxifier;
    }
    
    
    public function
        testGetXPathQuery()
    {
        $this->assertFalse(empty(trim($this->getProxifier()->getXPathQuery())));
    }
    
    public function
        getElementsFromHtml(string $html, int $nbExcepted)
    {
        $document = new DOMDocument();
        $this->assertTrue($document->loadXML($html));
        $this->assertFalse(
            empty(trim($document->saveXML($document->documentElement)))
        );
        $elements = $this->proxifier->getElements($document);
        $this->assertEquals($elements->length, $nbExcepted);
    }
    
    public function
        getNoElementFromHtml(string $html)
    {
        $this->getElementsFromHtml($html, 0);
    }
    
    public function
        getOneElementFromHtml(string $html)
    {
        $this->getElementsFromHtml($html, 1);
    }
    
    public abstract function
        testGetNoElement();
    
    public abstract function
        testGetOneElement();
    
    public abstract function
        testProxifyDocumentWithEmptyAttribute();
    
    public abstract function
        testProxifyDocumentWithFilledAttribute();
}
