<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlImageStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testIsJpeg()
    {
        $this->assertTrue(UrlImageStringUtils::isJpeg('img.jpg'));
        $this->assertTrue(UrlImageStringUtils::isJpeg('img.JPG'));
        $this->assertTrue(UrlImageStringUtils::isJpeg('img.jpeg'));
        $this->assertTrue(UrlImageStringUtils::isJpeg('img.JPEG'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.png'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.PNG'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.gif'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.GIF'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.webp'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.WEBP'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.svg'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.SVG'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('img.xml'));
        $this->assertFalse(UrlImageStringUtils::isJpeg('.'));
        $this->assertFalse(UrlImageStringUtils::isJpeg(''));
    }
    
    public function
        testIsPng()
    {
        $this->assertTrue(UrlImageStringUtils::isPng('img.png'));
        $this->assertTrue(UrlImageStringUtils::isPng('img.PNG'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.jpg'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.JPG'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.jpeg'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.JPEG'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.gif'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.GIF'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.webp'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.WEBP'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.svg'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.SVG'));
        $this->assertFalse(UrlImageStringUtils::isPng('img.xml'));
        $this->assertFalse(UrlImageStringUtils::isPng('.'));
        $this->assertFalse(UrlImageStringUtils::isPng(''));
    }
    
    public function
        testIsWebp()
    {
        $this->assertTrue(UrlImageStringUtils::isWebp('img.webp'));
        $this->assertTrue(UrlImageStringUtils::isWebp('img.WEBP'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.jpg'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.JPG'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.jpeg'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.JPEG'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.png'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.PNG'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.gif'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.GIF'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.svg'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.SVG'));
        $this->assertFalse(UrlImageStringUtils::isWebp('img.xml'));
        $this->assertFalse(UrlImageStringUtils::isWebp('.'));
        $this->assertFalse(UrlImageStringUtils::isWebp(''));
    }
}
