<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierFormGetTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttributeWithHtmlString(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier,
            string $html
        )
    {
        $document = new DOMDocument();
        $testCase->assertTrue($document->loadHTML($html));
        $htmlBefore = $document->saveHTML();
        $testCase->assertEquals($proxifier->proxifyDocument($document), 0);
        $htmlAfter  = $document->saveHTML();
        $testCase->assertEquals($htmlBefore, $htmlAfter);
    }
    
    public static function
        proxifyDocumentWithEmptyAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyDocumentWithEmptyAttributeWithHtmlString(
            $testCase, $proxifier,
            '<html><body></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeWithHtmlString(
            $testCase, $proxifier,
            '<html><body><form method="get"></form></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeWithHtmlString(
            $testCase, $proxifier,
            '<html><body><form method="get">'.
            '<input type="text" name="question" id="question" />'.
            '<input type="submit" />'
            .'</form></body></html>'
        );
        self::proxifyDocumentWithEmptyAttributeWithHtmlString(
            $testCase, $proxifier,
            '<html><body><form method="get" action="">'.
            '<input type="text" name="question" id="question" />'.
            '<input type="submit" />'
            .'</form></body></html>'
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttribute(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        $document = new DOMDocument();
        $testCase->assertTrue(
            $document->loadHTML(
                '<html><body><form method="get" action="form-controller.php">'.
                '<input type="text" name="question" id="question" />'.
                '<input type="submit" />'
                .'</form></body></html>'
            )
        );
        $htmlBefore = $document->saveHTML();
        $testCase->assertGreaterThanOrEqual(
            1, $proxifier->proxifyDocument($document)
        );
        $htmlAfter  = $document->saveHTML();
        $testCase->assertNotEquals($htmlBefore, $htmlAfter);
        $testCase->assertNotNull(
            $document->getElementById(
                ProxifierGetParametersUtils::DEFAULT_KEY_TO_PROXIFY
            )
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierFormGet();
    }
    
    
    public function
        getProxifier() : DomDocumentProxifierAbstract
    {
        return $this->proxifier;
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttribute(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttribute(
            $this, $this->getProxifier()
        );
    }
}
