<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierSourceSrcSetTest
    extends PHPUnit_Framework_TestCase
{
    public static function
        proxifyDocumentWithEmptyAttr(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><img srcset="" alt="" /></body></html>',
            new DomDocumentImportExport()
        );
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><img srcset=" " alt="" /></body></html>',
            new DomDocumentImportExport()
        );
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><picture>'.
            '<source srcset="" />'
            .'</picture></body></html>',
            new DomDocumentImportExport()
        );
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body><picture>'.
            '<source srcset=" " />'
            .'</picture></body></html>',
            new DomDocumentImportExport()
        );
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><body>'.
            '<source srcset="img1.png 1x, img2.png 2x" />'
            .'</body></html>',
            new DomDocumentImportExport()
        );
    }
    
    public static function
        proxifyDocumentWithFilledAttr(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><body><picture>'.
            '<source srcset="image.png" />'
            .'</picture></body></html>',
            '/html/body//picture/source[string(@srcset)]',
            'srcset',
            new DomDocumentImportExport()
        );
        
        $html =
              '<html><body><picture>'.
              '<source srcset="img1.png 1x, img2.png 2x" />'
              .'</picture></body></html>';
        $document = new DOMDocument();
        
        $testCase->assertTrue($document->loadXML($html));
        $htmlBefore = $document->saveXML();
        $testCase->assertFalse(empty(trim($htmlBefore)));
        
        $testCase->assertEquals($proxifier->proxifyDocument($document), 1);
        $htmlAfter  = $document->saveXML();
        $testCase->assertFalse(empty(trim($htmlAfter)));
        
        $testCase->assertNotEquals($htmlBefore, $htmlAfter);
        $testCase->assertGreaterThan(
            strlen($htmlBefore) + 2 * strlen($proxifier->getUrl()),
            strlen($htmlAfter)
        );
    }
    
    
    private $proxifier;
    
    
    public function
        __construct()
    {
        $this->proxifier = new HtmlDocumentProxifierSourceSrcSet();
    }
    
    
    public function
        getProxifier() : DomDocumentProxifierAbstract
    {
        return $this->proxifier;
    }
    
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocumentWithEmptyAttr(
            $this, $this->getProxifier()
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $this,
            $this->getProxifier(),
            '<html><body>'.
            '<img srcset="img1.png 1x, img2.png 2x" alt="" />'
            .'</body></html>'
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocumentWithFilledAttr(
            $this, $this->getProxifier()
        );
    }
}
