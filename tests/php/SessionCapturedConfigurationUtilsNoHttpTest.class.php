<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class SessionCapturedConfigurationUtilsNoHttpTest
    extends PHPUnit_Framework_TestCase
{
    public function
        createFromMapAndCheckSaveUndefined(array $aMap)
    {
        $config = SessionCapturedConfigurationUtils::createFromMap($aMap);
        $this->assertTrue($config->hasToSave()->isUndefined());
    }
    
    public function
        testCreateFromMapAndCheckSave()
    {
        $this->createFromMapAndCheckSaveUndefined(array());
        $this->createFromMapAndCheckSaveUndefined(array('save' => ''));
        $this->createFromMapAndCheckSaveUndefined(
            array('save-session' => '')
        );
        $this->createFromMapAndCheckSaveUndefined(
            array('save-cookies' => 'true')
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('save-session' => 'true')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('save-session' => 'yes')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('save-session' => 'on')
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('save-session' => 'false')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('save-session' => 'no')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('save-session' => 'off')
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured' => array('save' => 'on'))
        );
        $this->assertTrue($configuration->hasToSave()->isTrue());
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured' => array('save' => 'off'))
        );
        $this->assertTrue($configuration->hasToSave()->isFalse());
    }
    
    public function
        testCreateFromMapAndCheckCreateAutomatically()
    {
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array()
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isUndefined()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => '')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isUndefined()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => 'true')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isTrue()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => 'yes')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isTrue()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => 'on')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isTrue()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => 'false')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isFalse()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => 'no')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isFalse()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-create-auto' => 'off')
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isFalse()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured' => array('create-auto' => 'on'))
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isTrue()
        );
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured' => array('create-auto' => 'off'))
        );
        $this->assertTrue(
            $configuration->hasToCreateAutomatically()->isFalse()
        );
    }
    
    public function
        testCreateFromMapAndCheckDirectory()
    {
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-directory' => '/tmp')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-dir' => '/tmp')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-folder' => '/tmp')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-use-tmp' => 'true')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-use-temp-dir' => 'true')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-use-tmp-directory' => 'true')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
        
        $configuration = SessionCapturedConfigurationUtils::createFromMap(
            array('session-captured-use-temp-directory' => 'true')
        );
        $this->assertTrue($configuration->hasDirectory());
        $this->assertEquals(rtrim($configuration->getDirectory(), '/'), '/tmp');
    }
}
