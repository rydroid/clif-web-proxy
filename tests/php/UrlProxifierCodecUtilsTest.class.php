<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class UrlProxifierCodecUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetEncodedUrl()
    {
        $this->assertEquals(
            UrlProxifierCodecUtils::getEncodedUrl(''),
            ''
        );
        $this->assertNotEquals(
            UrlProxifierCodecUtils::getEncodedUrl('http://domain.org'),
            'http://domain.org'
        );
    }
    
    public function
        testEncodeParametersOfUrl()
    {
        $this->assertEquals(
            UrlProxifierCodecUtils::encodeParametersOfUrl(''),
            ''
        );
        $this->assertNotEquals(
            UrlProxifierCodecUtils::encodeParametersOfUrl(
                'http://domain.org?param1=value1&amp;param2=value2'
            ),
            'http://domain.org?param1=value1&amp;param2=value2'
        );
    }
}
