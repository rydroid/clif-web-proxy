<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class OptionnalBooleanTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testCreateUndefined()
    {
        $value = new OptionnalBoolean(OptionnalBooleanConst::UNDEFINED);
        $this->assertTrue($value->isUndefined());
        $this->assertFalse($value->isDefined());
        $this->assertFalse($value->isFalse());
        $this->assertFalse($value->isTrue());
    }
    
    public function
        testCreateFalse()
    {
        $value = new OptionnalBoolean(OptionnalBooleanConst::FALSE);
        $this->assertTrue($value->isFalse());
        $this->assertFalse($value->isTrue());
        $this->assertTrue($value->isDefined());
        $this->assertFalse($value->isUndefined());
    }
    
    public function
        testCreateTrue()
    {
        $value = new OptionnalBoolean(OptionnalBooleanConst::TRUE);
        $this->assertTrue($value->isTrue());
        $this->assertFalse($value->isFalse());
        $this->assertTrue($value->isDefined());
        $this->assertFalse($value->isUndefined());
    }
}
