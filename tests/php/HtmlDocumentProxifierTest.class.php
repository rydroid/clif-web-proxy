<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class HtmlDocumentProxifierTest
    extends UrlProxifierGenericTest
{
    public function
        __construct()
    {
        parent::__construct(new HtmlDocumentProxifier());
    }
    
    
    public function
        testProxifyDocumentWithEntity()
    {
        HtmlDocumentProxifierBodyOnlyTest::proxifyDocumentWithEntity(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        HtmlDocumentProxifierBodyOnlyTest::proxifyDocumentWithEmptyAttribute(
            $this, $this->getProxifier()
        );
        
        HtmlDocumentProxifierHeadOnlyTest::proxifyDocumentWithEmptyAttribute(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        HtmlDocumentProxifierBodyOnlyTest::proxifyDocumentWithFilledAttribute(
            $this, $this->getProxifier()
        );
        
        HtmlDocumentProxifierHeadOnlyTest::proxifyDocumentWithFilledAttribute(
            $this, $this->getProxifier()
        );
    }
    
    public function
        testProxifyDocumentWithCss()
    {
        HtmlDocumentProxifierStyleTest::proxifyString(
            $this, $this->getProxifier()
        );
    }
}
