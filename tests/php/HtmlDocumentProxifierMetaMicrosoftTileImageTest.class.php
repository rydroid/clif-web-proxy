<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('DomDocumentProxifierWithXPathAbstractTest.class.php');


final class HtmlDocumentProxifierMetaMicrosoftTileImageTest
    extends DomDocumentProxifierWithXPathAbstractTest
{
    public static function
        proxifyDocWithEmptyAttr(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta name="" content="" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta name="" content="image.png" />'
            .'</head></html>'
        );
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta name="msapplication-TileImage" content="" />'
            .'</head></html>'
        );
    }
    
    public static function
        proxifyDocWithFilledAttr(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        DomDocumentProxifierAttributeUtils::proxifyDocumentWithFilledAttribute(
            $testCase,
            $proxifier,
            '<html><head>'.
            '<meta name="msapplication-TileImage" content="image.png" />'
            .'</head></html>',
            '/html/head/meta', 'content',
            new DomDocumentImportExport()
        );
    }
    
    
    public function
        __construct()
    {
        parent::__construct(new HtmlDocumentProxifierMetaMicrosoftTileImage());
    }
    
    
    public function
        testGetNoElement()
    {
        $this->getNoElementFromHtml('<html></html>');
        $this->getNoElementFromHtml('<html><head></head></html>');
        $this->getNoElementFromHtml(
            '<html><head><meta content="" /></head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head><meta content="image.png" /></head></html>'
        );
        $this->getNoElementFromHtml(
            '<html><head>'.
            '<meta name="msapplication-TileImage" content="" />'
            .'</head></html>'
        );
    }
    
    public function
        testGetOneElement()
    {
        $this->getOneElementFromHtml(
            '<html><head>'.
            '<meta name="msapplication-TileImage" content="image.png" />'
            .'</head></html>'
        );
    }
    
    public function
        testProxifyDocumentWithEmptyAttribute()
    {
        self::proxifyDocWithEmptyAttr($this, $this->getProxifier());
        
        HtmlDocumentProxifierAttributeUtils::proxifyDocumentWithEmptyAttribute(
            $this,
            $this->getProxifier(),
            '<html><head>'.
            '<meta name="msapplication-starturl" content="index.html" />'
            .'</head></html>'
        );
    }
    
    public function
        testProxifyDocumentWithFilledAttribute()
    {
        self::proxifyDocWithFilledAttr($this, $this->getProxifier());
    }
}
