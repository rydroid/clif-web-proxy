<?php
/* Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class BooleanStringUtilsTest
    extends PHPUnit_Framework_TestCase
{
    const FALSE_STRINGS = [
        'false', 'FALSE', 'False', ' false', 'false ', ' false '
    ];
    const TRUE_STRINGS = [
        'true', 'TRUE', 'True', ' true', 'true ', ' true '
    ];
    const NOT_BOOL_STRINGS = [
        'fals', 'alse', 'tru', 'rue', 'falsetrue', 'false true'
    ];
    
    
    public function
        testIsFalse()
    {
        foreach(self::NOT_BOOL_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isFalse($value));
        }
        foreach(self::FALSE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isFalse($value));
        }
        foreach(self::TRUE_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isFalse($value));
        }
    }
    
    public function
        testIsTrue()
    {
        foreach(self::NOT_BOOL_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isTrue($value));
        }
        foreach(self::FALSE_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isTrue($value));
        }
        foreach(self::TRUE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isTrue($value));
        }
    }
    
    public function
        testIsBoolean()
    {
        foreach(self::NOT_BOOL_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isBoolean($value));
        }
        foreach(self::FALSE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isBoolean($value));
        }
        foreach(self::TRUE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isBoolean($value));
        }
    }
    
    
    public function
        testIsFalseExtended()
    {
        foreach(self::NOT_BOOL_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isFalseExtended($value));
        }
        foreach(self::FALSE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isFalseExtended($value));
        }
        foreach(self::TRUE_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isFalseExtended($value));
        }
        
        $this->assertTrue(BooleanStringUtils::isFalseExtended('non'));
        $this->assertFalse(BooleanStringUtils::isFalseExtended('oui'));
        $this->assertTrue(BooleanStringUtils::isFalseExtended('off'));
        $this->assertFalse(BooleanStringUtils::isFalseExtended('on'));
    }
    
    public function
        testIsTrueExtended()
    {
        foreach(self::NOT_BOOL_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isTrueExtended($value));
        }
        foreach(self::FALSE_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isTrueExtended($value));
        }
        foreach(self::TRUE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isTrueExtended($value));
        }
        
        $this->assertFalse(BooleanStringUtils::isTrueExtended('non'));
        $this->assertTrue(BooleanStringUtils::isTrueExtended('oui'));
        $this->assertFalse(BooleanStringUtils::isTrueExtended('off'));
        $this->assertTrue(BooleanStringUtils::isTrueExtended('on'));
    }
    
    public function
        testIsBooleanExtended()
    {
        foreach(self::NOT_BOOL_STRINGS as $value)
        {
            $this->assertFalse(BooleanStringUtils::isBooleanExtended($value));
        }
        foreach(self::FALSE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isBooleanExtended($value));
        }
        foreach(self::TRUE_STRINGS as $value)
        {
            $this->assertTrue(BooleanStringUtils::isBooleanExtended($value));
        }
        
        $this->assertTrue(BooleanStringUtils::isBooleanExtended('oui'));
        $this->assertTrue(BooleanStringUtils::isBooleanExtended('non'));
        $this->assertTrue(BooleanStringUtils::isBooleanExtended('off'));
        $this->assertTrue(BooleanStringUtils::isBooleanExtended('on'));
    }
}
