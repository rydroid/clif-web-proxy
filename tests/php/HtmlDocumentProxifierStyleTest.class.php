<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');
require_once('UrlProxifierGenericTest.class.php');


final class HtmlDocumentProxifierStyleTest
    extends UrlProxifierGenericTest
{
    public static function
        proxifyStringNothing(
            PHPUnit_Framework_TestCase $testCase,
            DomDocumentProxifierAbstract $proxifier)
    {
        HtmlDocumentProxifierStyleTagTest::proxifyStringNothing(
            $testCase, $proxifier
        );
        HtmlDocumentProxifierStyleAttributeTest::proxifyStringNothing(
            $testCase, $proxifier
        );
    }
    
    public static function
        proxifyStringSuccess(
            UrlProxifierGenericTest $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        HtmlDocumentProxifierStyleTagTest::proxifyStringSuccess(
            $testCase, $proxifier
        );
        HtmlDocumentProxifierStyleAttributeTest::proxifyStringSuccess(
            $testCase, $proxifier
        );
    }
    
    public static function
        proxifyString(
            UrlProxifierGenericTest $testCase,
            DomDocumentProxifierAbstract $proxifier
        )
    {
        self::proxifyStringNothing($testCase, $proxifier);
        self::proxifyStringSuccess($testCase, $proxifier);
    }
    
    
    public function
        __construct()
    {
        parent::__construct(new HtmlDocumentProxifierStyle());
    }
    
    
    public function
        testProxifyStringNothing()
    {
        self::proxifyStringNothing($this, $this->getProxifier());
    }
    
    public function
        testProxifyStringSuccess()
    {
        self::proxifyStringSuccess($this, $this->getProxifier());
    }
}
