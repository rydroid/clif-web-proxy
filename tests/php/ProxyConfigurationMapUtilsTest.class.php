<?php
/*
 * Copyright (C) 2017  Nicola Spanti <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);


require_once('PHPUnit/Autoload.php');


final class ProxyConfigurationMapUtilsTest
    extends PHPUnit_Framework_TestCase
{
    public function
        testGetKeyOfUrl()
    {
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfUrl(
                array()
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfUrl(
                array('proxy' => '')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfUrl(
                array('proxy-url' => '')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfUrl(
                array('proxy-port' => '80')
            )
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getKeyOfUrl(
                array('proxy-url' => 'https://example.net')
            ),
            'proxy-url'
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getKeyOfUrl(
                array('url-of-proxy' => 'https://example.net')
            ),
            'url-of-proxy'
        );
    }
    
    public function
        testGetUrl()
    {
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getUrl(
                array()
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getUrl(
                array('proxy' => '')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getUrl(
                array('proxy-url' => '')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getUrl(
                array('proxy-port' => '80')
            )
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getUrl(
                array('proxy-url' => 'https://example.net')
            ),
            'https://example.net'
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getUrl(
                array('url-of-proxy' => 'https://example.net')
            ),
            'https://example.net'
        );
    }
    
    public function
        testGetKeyOfPort()
    {
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array()
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy' => '')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy-url' => '')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy-url' => 'https://example.net')
            )
        );
        $this->assertEmpty(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy-port' => '')
            ), -1
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy-port' => '80')
            ), 'proxy-port'
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy-port' => '8080')
            ), 'proxy-port'
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getKeyOfPort(
                array('proxy-port-number' => '80')
            ), 'proxy-port-number'
        );
    }
    
    public function
        testGetPort()
    {
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array()
            ), -1
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy' => '')
            ), -1
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy-url' => '')
            ), -1
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy-url' => 'https://example.net')
            ), -1
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy-port' => '')
            ), -1
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy-port' => '80')
            ), 80
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy-port' => '8080')
            ), 8080
        );
        $this->assertEquals(
            ProxyConfigurationMapUtils::getPort(
                array('proxy-port-number' => '80')
            ), 80
        );
    }
}
