# Credits

## Creation

- Nicola Spanti has created this program. He did it during an internship at Orange Labs with Bruno Dillenseger as the supervisor.
- Bruno Dillenseger is one of the developer of CLIF (that is a recursive acronym : "CLIF is a Load Injection Framework"). He needed a better free/libre tool to capture web session and have a way to get the result in the format of ISAC (recursivity again : "ISAC is a Scenario Architecture for CLIF"). He asked Orange (the company in which he had a paid job) to propose an internship for his aim and accepted Nicola Spanti for it.
- Orange is the company that accepted the internship and paid Nicola Spanti and his supervisor for it.
- ENSICAEN (an engineering school in France) has accepted the internship of Nicola Spanti. Nicola Spanti had to do an internship to validate his final year in this school and get the corresponding diploma in IT.
- Other persons and groups could have been credited, but I (Nicola Spanti) do not know if they want to be credited and it would be lengthy.
